# Rust Lox Interpreter

### Vendor Extensions

* Block Comments using `/*` and `*/` 
* Conditional ternary operator: `someBool ? "True" : "False" `
* Implicit string conversions in binary operators. The following are all valid:
  * `print "" + 42;` prints "42"
  * `print 42 + "";` prints "42"
  * `print true + " or " + false;` prints "true or false"
  * `break` expressions may be used in a while or for loop
  * `continue` expressions may be used in a while loop, but is buggy in a for loop.


### Additional Implementation Notes
* Variables default initialize to nil
* The east side of a variable declaration is evaluated before the variable is instantiated.
  * This will produce a runtime error:
    * `var a = a;` 
  * This is well-formed:
    *     var a = "Hello, ";
          {
              var a = a + "World!"; 
              // a == "Hello, World!"
          }
  

### Ideas for extensions and other ToDos:
* Create a function that returns/prints the stack
* Implement exceptions 
  * The logic of this should be similar to the logic of `break` expressions 

### Known issues
* Continue expressions don't work in for loops
  * The expression that is evaluated at the end of each iteration is skipped which can lead to infinite loops
  * A possible solution would be to implement a for statement instead of transforming the source into a while loop