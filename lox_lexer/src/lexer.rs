use std::str::FromStr;
use crate::scanner;
use lox_library::{object, token};
use lox_library::lox_error::Error;
use lox_library::token::{TokenContext, TokenType};

fn is_alphanumeric(c: char)->bool {
    c.is_alphanumeric() || c == '_'
}
fn is_alpha(c: char)->bool {
    c.is_alphanumeric() || c == '_'
}

fn skip_to_eol(s: &mut scanner::Scanner) {
    loop {
        match s.read_next(){
            Some('\n') | None => return,
            _ => ()
        }
    }
}
/// Suggested Vendor Extension to parse block comments
fn parse_block_comment(s: &mut scanner::Scanner) ->Result<(), Error> {

    let loc_char = s.loc_char();

    let mut depth = 1u32;
    loop {
        if      s.match_str("/*"){ depth += 1;}
        else if s.match_str("*/"){ depth -= 1;}
        else if !s.advance()     { return Err(Error {loc_char, error_string: "Unable to find closing */".to_string()})}

        if depth == 0 {
            return Ok(());
        }
    }
}

fn skip_whitespace(s: &mut scanner::Scanner) ->Option<token::TokenType> {
    loop {
        match s.peek_next(0){
            Some(c) => {
                if c.is_whitespace() {s.read_next();} else {return None;}
            }
            None => return Some(token::TokenType::Eof),
        }
    }
}

fn parse_string(s: &mut scanner::Scanner) ->Result<object::Object, Error> {

    let loc_char = s.loc_byte();
    let mut ret = String::new();

    loop {
        match s.read_next() {
            Some('"') => return Ok(object::Object::Str(ret)),
            Some('\\') => match s.read_next() {
                Some('n')  => ret.push('\n'),
                Some('r')  => ret.push('\r'),
                Some('t')  => ret.push('\t'),
                Some('\\') => ret.push('\\'),
                Some('\'') => ret.push('\''),
                Some('\"') => ret.push('\"'),
                Some(_) => (), // Should issue a warning for invalid literal
                None => return Err(Error {error_string:"Found EOF while parsing string".to_string(), loc_char}),
            }
            Some(c) => ret.push(c),
            None => return Err(Error {error_string:"Found EOF while parsing string".to_string(), loc_char}),
        }
    }
}


fn parse_number(s: &mut scanner::Scanner) -> Result<object::Object, Error> {

    let loc_byte = s.loc_byte();
    let loc_char = s.loc_byte();

    let skip_digits = |scn: &mut scanner::Scanner| {
        loop {
            match scn.peek_next(0){
                Some(c) => if c.is_digit(10) { scn.read_next(); } else {break}
                None => break
            }
        }
    };

    { // Parse the leading digits
        skip_digits(s);
    }

    { // Parse the dot and next digits
        let next0 = s.peek_next(0);
        let next1 = s.peek_next(1);

        if next0.unwrap_or('\0') == '.' && next1.unwrap_or('\0').is_digit(10) {
            assert_eq!(s.read_next().unwrap(), '.'); // discard the dot
            skip_digits(s);
        }
    }

    // Vendor Extension
    // My implementation allows numbers written in scientific notation
    // e.g 42.12e-3 or 2.9979e8
    //

    { // Parse the exponent
        let next0 = s.peek_next(0);
        let next1 = s.peek_next(1);
        if next0 == Some('e')  && next1.unwrap_or('\0').is_digit(10) {
            assert_eq!(s.read_next().unwrap(), 'e'); // discard the e
            skip_digits(s);
        }

        let next2 = s.peek_next(2);
        if next0 == Some('e') && next1 == Some('-') && next2.unwrap_or('\0').is_digit(10) {
            assert_eq!(s.read_next().unwrap(), 'e'); // discard the e
            assert_eq!(s.read_next().unwrap(), '-'); // discard the -
            skip_digits(s);
        }
    }

    match f64::from_str(s.create_substring(loc_byte, s.loc_byte() )) {
        Ok(c) => Ok(object::Object::Num(c)),
        Err(e) => Err(Error { loc_char, error_string:e.to_string()})
    }
}

fn parse_identifier(s: &mut scanner::Scanner) -> Result<(), Error> {

    while is_alphanumeric(s.peek_next(0).unwrap_or('\0')) {
        s.advance();
    }
    Ok(())
}

//
// // This function will scan from the start point and return a token::Token if one begins at point loc
// // Returning None indicates that no lexeme was present at point loc in the source code
//
fn maybe_scan_token<'a>(s: &mut scanner::Scanner<'a>) -> Result<Option<token::TokenWithContext<'a>>, Error>
{
    skip_whitespace(s);

    let start_byte = s.loc_byte();
    let start_char = s.loc_char();

    let c = match s.peek_next(0) {
        Some(x) => x,
        None => return Ok(Some(token::TokenWithContext {
                                token_type    : token::TokenType::Eof,
                                token_name    : String::new(),
                                representation: None,
                                context       : Some(TokenContext{
                                    lexeme      : "",
                                    byte_count  : start_byte,
                                    char_count  : start_char,
                                    source      : s.source,
                                }),
        }))
    };


    let mut token_rep : Option<object::Object> = None;
    let mut token_type :Option<token::TokenType> = None;
    let mut token_name;

    if      s.match_str("!=") {token_type = Some(token::TokenType::BangEqual)    }
    else if s.match_str("==") {token_type = Some(token::TokenType::EqualEqual)   }
    else if s.match_str("<=") {token_type = Some(token::TokenType::LessEqual)    }
    else if s.match_str(">=") {token_type = Some(token::TokenType::GreaterEqual) }
    else if s.match_str("//") { skip_to_eol(s)                           }
    else if s.match_str("/*") {parse_block_comment(s)?                    }

    else if s.match_char('(') {token_type = Some(token::TokenType::LeftParen)    }
    else if s.match_char(')') {token_type = Some(token::TokenType::RightParen)   }
    else if s.match_char('{') {token_type = Some(token::TokenType::LeftBrace)    }
    else if s.match_char('}') {token_type = Some(token::TokenType::RightBrace)   }
    else if s.match_char(',') {token_type = Some(token::TokenType::Comma)        }
    else if s.match_char('.') {token_type = Some(token::TokenType::Dot)          }
    else if s.match_char('-') {token_type = Some(token::TokenType::Minus)        }
    else if s.match_char('+') {token_type = Some(token::TokenType::Plus)         }
    else if s.match_char(';') {token_type = Some(token::TokenType::Semicolon)    }
    else if s.match_char(':') {token_type = Some(token::TokenType::Colon)        }
    else if s.match_char('%') {token_type = Some(token::TokenType::Remainder)    }
    else if s.match_char('*') {token_type = Some(token::TokenType::Star)         }
    else if s.match_char('!') {token_type = Some(token::TokenType::Bang)         }
    else if s.match_char('=') {token_type = Some(token::TokenType::Equal)        }
    else if s.match_char('<') {token_type = Some(token::TokenType::Less)         }
    else if s.match_char('>') {token_type = Some(token::TokenType::Greater)      }
    else if s.match_char('/') {token_type = Some(token::TokenType::Slash)        }
    else if s.match_char('?') {token_type = Some(token::TokenType::Question)     }

    else if s.match_char('"') {token_type = Some(token::TokenType::String);     token_rep = Some(parse_string(s)?) }
    else if c.is_digit(10)    {token_type = Some(token::TokenType::Number);     token_rep = Some(parse_number(s)?) }
    else if is_alpha(c)       {token_type = Some(token::TokenType::Identifier); parse_identifier(s)?              }
    else {
        skip_to_eol(s);
        return Err(Error {
            loc_char:start_char,
            error_string:format!("Unexpected character: '{}'", c.to_string())})
    };

    let lexeme = &s.create_substring(start_byte, s.loc_byte());
    token_name = lexeme.to_string();

    if token_type.is_none() {
        return Ok(None);
    }

    if token_type == Some(token::TokenType::Identifier) {
        match token::keyword_map(lexeme) {
            Some(t) => token_type = Some(t),
            _       => token_name = lexeme.to_string(),
        }
    }

    if token_type == Some(TokenType::True){
        token_rep = Some(object::Object::Bool(true));
    }
    if token_type == Some(TokenType::False){
        token_rep = Some(object::Object::Bool(false));
    }
    if token_type == Some(TokenType::Nil){
        token_rep = Some(object::Object::Nil(()));
    }
    Ok(Some(token::TokenWithContext{
        token_type    : token_type.unwrap(),
        token_name    : token_name,
        representation: token_rep,
        context       : Some(TokenContext{
            lexeme      : lexeme,
            byte_count  : start_byte,
            char_count  : start_char,
            source      : s.source,
        }),
    }))
}

fn scan_token<'a>(s: &mut scanner::Scanner<'a>) -> Result<token::TokenWithContext<'a>, Error>{

    loop  {
        let t = maybe_scan_token(s)?;
        if t.is_some() {
            return Ok(t.unwrap());
        }
    }

}


pub fn scan_tokens(source: &str) ->Result<Vec<token::TokenWithContext>, Vec<Error>> {
    let mut s = scanner::Scanner::new(source);
    let mut ret = Vec::new();
    let mut err = Vec::new();
    loop {

        match scan_token(&mut s){
            Ok(t) => ret.push(t),
            Err(e) => err.push(e),
        };

        if  ret.last().is_some() && ret.last().unwrap().token_type == token::TokenType::Eof{
            if !err.is_empty(){
                return Err(err);
            }
            return Ok(ret);
        }
    }
}




