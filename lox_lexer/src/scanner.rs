
#[derive(Debug)]
pub struct Scanner<'a>{
    pub source   : &'a str,
    loc_char : usize,
    loc_byte : usize,
}

impl<'a> Scanner<'a> {

    pub fn new(source: &str) -> Scanner{
        Scanner{ source, loc_char: 0, loc_byte: 0}
    }

    pub fn loc_char(&self) -> usize {
        self.loc_char
    }

    pub fn loc_byte(&self) -> usize {
        self.loc_byte
    }

    pub fn create_substring(&self, start:usize, end:usize) -> &'a str {
        &self.source[start..end]
    }

    pub fn advance(&mut self) -> bool {
        let remaining_bytes = &self.source[self.loc_byte..];

        if remaining_bytes.len() == 0 {
            return false;
        }

        let c = remaining_bytes.chars().nth(0).unwrap();
        self.loc_char += 1;
        self.loc_byte += c.len_utf8();
        true
    }

    pub fn read_next(&mut self) ->Option<char> {

        let ret = match self.peek_next(0) {
            Some(c) => c,
            None => return None,
        };
        self.advance();
        Some(ret)
    }

    pub fn peek_next(&self, offset: usize ) ->Option<char> {

        let substr_source = &self.source[self.loc_byte..];
        substr_source.chars().nth(offset)

    }

    pub fn match_char(&mut self, to_match: char) -> bool{

        let to_match_with = match self.peek_next(0){
            None => return false,
            Some(c) => c,
        };

        if to_match == to_match_with {
            self.advance();
            return true;
        }
        return false;
    }

    pub fn match_str(&mut self, to_match: &str) -> bool{

        let source_str = &self.source[self.loc_byte..];
        if source_str.starts_with(to_match) {
            for c in to_match.chars() {
                assert_eq!( c, self.read_next().unwrap() );
            }
            return true;
        }
        return false;
    }

}
