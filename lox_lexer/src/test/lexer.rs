
#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.

    use lox_library::{object, token};
    use crate::lexer::scan_tokens;

    type TT = token::TokenType;
    type TR = object::Object;


    #[test]
    fn test_scan_tokens() {
        let tokens_str = "(){}!!=,.-+;*".to_string();

        let tokens_scanned: Vec<token::TokenType> = scan_tokens(&tokens_str)
            .unwrap().iter()
            .map(|x| x.token_type)
            .collect();

        let tokens_gold = vec![token::TokenType::LeftParen,
                               token::TokenType::RightParen,
                               token::TokenType::LeftBrace,
                               token::TokenType::RightBrace,
                               token::TokenType::Bang,
                               token::TokenType::BangEqual,
                               token::TokenType::Comma,
                               token::TokenType::Dot,
                               token::TokenType::Minus,
                               token::TokenType::Plus,
                               token::TokenType::Semicolon,
                               token::TokenType::Star,
                               token::TokenType::Eof];

        assert_eq!(tokens_scanned, tokens_gold);
    }


    #[test]
    fn test_invalid_char() {
        let tokens_str = "(){}#,.-+;*".to_string();
        let vec_errs = scan_tokens(&tokens_str).err().unwrap();
        assert_eq!(vec_errs.len(), 1);
        assert_eq!(vec_errs[0].loc_char, 4);
    }


    #[test]
    fn test_whitespace_comments() {

        let source = "\
        \
            // a nice short comment !! # \n
            func(/*meaning of life = */ 42 )
            func(/*meaning /* of */ life = */ 42 )
            \
        ".to_string();

        let tokens_scanned: Vec<token::TokenType> = scan_tokens(&source)
            .unwrap().iter()
            .map(|x| x.token_type)
            .collect();

        let tokens_gold = vec![TT::Identifier, TT::LeftParen, TT::Number, TT::RightParen, TT::Identifier, TT::LeftParen, TT::Number, TT::RightParen, TT::Eof];

        assert_eq!(tokens_scanned, tokens_gold);

    }


    #[test]
    fn test_symbols_comments() {

        let source =
            r#"// this is a comment
(( )){} // grouping stuff
!*+-/=<> <= == // operators
+"Here is a string"
"#.to_string();

        let tokens_scanned: Vec<token::TokenType> = scan_tokens(&source)
            .unwrap().iter()
            .map(|x| x.token_type)
            .collect();

        let tokens_gold = vec![TT::LeftParen, TT::LeftParen, TT::RightParen, TT::RightParen, TT::LeftBrace, TT::RightBrace, TT::Bang, TT::Star, TT::Plus, TT::Minus, TT::Slash, TT::Equal, TT::Less, TT::Greater, TT::LessEqual, TT::EqualEqual, TT::Plus, TT::String, TT::Eof];


        assert_eq!(tokens_scanned, tokens_gold);
    }
    #[test]
    fn test_string() {

        let source =
            r#"   "Hello" "World" "#.to_string();

        let tokens = scan_tokens(&source).unwrap();
        let tokens_scanned: Vec<token::TokenType> = tokens
            .iter()
            .map(|x| x.token_type)
            .collect();

        let values_scanned: Vec<&str> = tokens
            .iter()
            .filter(|x| x.representation.is_some() )
            .map(|x| if let TR::Str(res) = x.representation.as_ref().unwrap() {&res} else {""}  )
            .collect();

        let tokens_gold = vec![TT::String, TT::String, TT::Eof];

        let values_gold = vec!["Hello", "World"];

        assert_eq!(tokens_scanned, tokens_gold);
        assert_eq!(values_scanned, values_gold);
    }

    #[test]
    fn test_numbers() {

        let source =
            r#"12.23 42 1 2 3 0.12 42.45e-4 12e6 2.9979e8
"#.to_string();

        let tokens_scanned: Vec<token::TokenType> = scan_tokens(&source).unwrap()
            .iter()
            .map(|x| x.token_type)
            .collect();

        let values_scanned: Vec<f64> = scan_tokens(&source).unwrap()
            .drain(0..)
            .filter(|x| x.representation.is_some() )
            .map(|x| if let TR::Num(res) = x.representation.unwrap() {return res;} else {0f64}  )
            .collect();

        let tokens_gold = vec![TT::Number, TT::Number, TT::Number, TT::Number, TT::Number, TT::Number, TT::Number, TT::Number, TT::Number, TT::Eof];

        let values_gold = vec![12.23, 42f64, 1f64, 2f64, 3f64, 0.12f64, 42.45e-4, 12e6, 2.9979e8];

        assert_eq!(tokens_scanned, tokens_gold);
        assert_eq!(values_scanned, values_gold);
    }

    #[test]
    fn test_identifiers() {

        let source =
            r#" hello _identi42 "#.to_string();

        let tokens_scanned: Vec<token::TokenType> = scan_tokens(&source).unwrap()
            .iter()
            .map(|x| x.token_type)
            .collect();

        let tokens_gold = vec![TT::Identifier, TT::Identifier, TT::Eof];

        assert_eq!(tokens_scanned, tokens_gold);
    }

    #[test]
    fn test_identifier_keywords() {

        let source =
            r#" hello world for if nil "#.to_string();

        let tokens_scanned: Vec<token::TokenType> = scan_tokens(&source).unwrap()
            .iter()
            .map(|x| x.token_type)
            .collect();

        let tokens_gold = vec![TT::Identifier, TT::Identifier, TT::For, TT::If, TT::Nil, TT::Eof];

        assert_eq!(tokens_scanned, tokens_gold);
    }

}

