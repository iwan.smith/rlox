use std::fmt::{Debug, Formatter};
use crate::token::TokenWithContext;

// Struct to represent an error
pub struct Error {
    pub loc_char: usize,
    pub error_string: String
}

impl Error {
    fn report( &self ) -> String{
        format!("Error at char: {}. {}", self.loc_char, self.error_string)
    }
}

impl Debug for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.report())
    }
}


pub fn source_substring<'a>(t1: &'a TokenWithContext, t2: &'a TokenWithContext) -> &'a str{

    // We need to be certain that the two tokens come from the same string!
    match [&t1.context, &t2.context] {
        [Some(ctx1), Some(ctx2)] => {
            assert!(std::ptr::eq(ctx1.source, ctx2.source));
            &ctx1.source[ctx1.byte_count..ctx2.byte_count]
        }
        _ => {
            ""
        }
    }
}

#[derive(PartialEq)]
enum HowToFormat{
    UpToToken,
    FromToken,
    WholeLine
}

fn format_line(t: &TokenWithContext, htf: HowToFormat) -> String {

    if t.context.is_none() {
        return String::new();
    }
    let ctx = t.context.as_ref().unwrap();

    let source = ctx.source;
    let mut line = String::new();

    for (count, c) in source.chars().enumerate() {

        if htf == HowToFormat::UpToToken && ctx.char_count == count {
            break;
        }
        if htf == HowToFormat::FromToken && ctx.char_count == count {
            line = String::new();
        }

        line.push(c);
        if c == '\n' {
            if count > ctx.char_count  {
                break;
            }
            line = String::new();
        }
    }
    line

}

pub fn format_line_containing_token(t: &TokenWithContext) -> String {
    format_line(t, HowToFormat::WholeLine)
}

pub fn format_line_up_to_token(t: &TokenWithContext) -> String {
    format_line(t, HowToFormat::UpToToken)
}

pub fn format_line_from_token(t: &TokenWithContext) -> String {
    format_line(t, HowToFormat::FromToken)
}