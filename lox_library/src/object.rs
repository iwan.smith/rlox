use std::fmt;

use std::ops;
use std::cmp;
use std::cmp::Ordering;

#[derive(Debug,PartialEq,Clone)]
pub enum Object {
    Str  (String),
    Num  (f64),
    Bool (bool),
    Nil  (())
}

impl fmt::Display for Object {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self{
            Object::Str(s)  => write!(f, "\"{}\"", s),
            Object::Num(n)  => write!(f, "{}", n),
            Object::Bool(b) => write!(f, "{}", b),
            Object::Nil(()) => write!(f, "nil"),
        }
    }
}



impl Object {

    pub fn new_str (str: String) -> Object { Object::Str (str) }
    pub fn new_num (num: f64   ) -> Object { Object::Num (num) }
    pub fn new_bool(b  : bool  ) -> Object { Object::Bool(b  ) }
    pub fn new_nil (           ) -> Object { Object::Nil (() ) }

    fn maybe_partial_cmp(&self, rhs: &Self) -> Result<cmp::Ordering, String> {
        match (self, rhs) {
            (Object::Str (s1), Object::Str (s2)) => Ok( s1.partial_cmp(s2).unwrap() ),
            (Object::Num (n1), Object::Num (n2)) => Ok( n1.partial_cmp(n2).unwrap() ),
            (Object::Bool(b1), Object::Bool(b2)) => Ok( b1.partial_cmp(b2).unwrap() ),
            _ => Err(format!("Unable to compare '{}' with '{}'", self, rhs))
        }
    }


    pub fn lt( &self, rhs: &Self ) -> Result<Object, String> {
        Ok( Object::Bool( self.maybe_partial_cmp(&rhs)? == Ordering::Less ) )
    }
    pub fn lt_eq( &self, rhs: &Self ) -> Result<Object, String> {
        Ok( Object::Bool( self.maybe_partial_cmp(&rhs)? != Ordering::Greater ) )
    }
    pub fn gt( &self, rhs: &Self ) -> Result<Object, String> {
        Ok( Object::Bool( self.maybe_partial_cmp(&rhs)? == Ordering::Greater ) )
    }
    pub fn gt_eq( &self, rhs: &Self ) -> Result<Object, String> {
        Ok( Object::Bool( self.maybe_partial_cmp(&rhs)? != Ordering::Less ) )
    }
    pub fn eq( &self, rhs: &Self ) -> Result<Object, String> {
        match (self, rhs) {
            (Object::Bool(b1), rhs) => Ok(Object::Bool(*b1 == rhs.is_truthy())),
            (lhs, Object::Bool(b2)) => Ok(Object::Bool(*b2 == lhs.is_truthy())),
            _ => Ok( Object::Bool( self.maybe_partial_cmp(&rhs)? == Ordering::Equal ) )
        }

    }
    pub fn neq( &self, rhs: &Self ) -> Result<Object, String> {
        let res = self.eq(rhs)?;
        !res
    }

    pub fn is_truthy(&self) -> bool {
        match self{
            Object::Bool(false) => false,
            Object::Nil(())     => false,
            _                   => true,
        }
    }


    pub fn to_string(&self) -> String {
        match self {
            Object::Str (s1) => s1.clone(),
            Object::Num (n1) => n1.to_string(),
            Object::Bool(b1) => b1.to_string(),
            Object::Nil (_)  => "nil".to_string()
        }
    }

}


impl ops::Add for Object{
    type Output = Result<Self,String>;

    fn add(self, rhs: Self) -> Result<Self,String> {
        match (&self, &rhs) {
            (Object::Str(s1), Object::Str(s2)) => Ok(Object::Str(s1.to_owned() + s2)),
            (Object::Num(s1), Object::Num(s2)) => Ok(Object::Num(s1 + s2)),
            (Object::Str(s1), rhs_obj)         => Ok(Object::Str(s1.to_owned() +  &*rhs_obj.to_string() ) ),
            (lhs,             Object::Str(s2)) => Ok(Object::Str(lhs.to_string() + s2 ) ),
            _ => Err(format!("Don't know how to add: '{}' + '{}'", self, rhs))
        }
    }
}

impl ops::Sub for Object{
    type Output = Result<Self,String>;

    fn sub(self, rhs: Self) -> Result<Self,String> {
        match (&self, &rhs) {
            (Object::Num(s1), Object::Num(s2)) => Ok(Object::Num(s1 - s2)),
            _ => Err(format!("Don't know how to subtract: '{}' - '{}'", self, rhs))
        }
    }
}

impl ops::Mul for Object{
    type Output = Result<Self,String>;

    fn mul(self, rhs: Self) -> Result<Self,String> {
        match (&self, &rhs) {
            (Object::Num(s1), Object::Num(s2)) => Ok(Object::Num(s1 * s2)),
            _ => Err(format!("Don't know how to multiply: '{}' * '{}'", self, rhs))
        }
    }
}

impl ops::Div for Object{
    type Output = Result<Self,String>;

    fn div(self, rhs: Self) -> Result<Self,String> {
        match (&self, &rhs) {
            (Object::Num(s1), Object::Num(s2)) => Ok(Object::Num(s1 / s2)),
            _ => Err(format!("Don't know how to Divide: '{}' / '{}'", self, rhs))
        }
    }
}

impl ops::Rem for Object{
    type Output = Result<Self,String>;

    fn rem(self, rhs: Self) -> Result<Self,String> {
        match (&self, &rhs) {
            (Object::Num(s1), Object::Num(s2)) => Ok(Object::Num(s1 % s2)),
            _ => Err(format!("Don't know how to get the Remainder: '{}' % '{}'", self, rhs))
        }
    }
}


impl ops::Neg for Object{
    type Output = Result<Self,String>;

    fn neg(self) -> Result<Self,String> {
        match self {
            Object::Num(n1)=> Ok(Object::Num(-n1)),
            _ => Err(format!("Don't know how to negate: - '{}'", self))
        }
    }
}

impl ops::Not for Object{
    type Output = Result<Self,String>;

    fn not(self) -> Result<Self,String> {
        match self {
            Object::Bool(b1)=> Ok(Object::Bool(!b1)),
            _ => Err(format!("Don't know how to logically negate: ! '{}'", self))
        }
    }
}
