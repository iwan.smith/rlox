
#[cfg(test)]
mod tests {

    use crate::{object, Object};


    #[test]
    fn object_tests() {
        assert_eq!(Object::Num(4.0) + Object::Num(5.0), Ok(Object::Num(9.0)));
        assert_eq!(Object::Num(4.0) - Object::Num(5.0), Ok(Object::Num(-1.0)));
        assert_eq!(Object::Num(4.0) * Object::Num(5.0), Ok(Object::Num(20.0)));
        assert_eq!(Object::Num(4.0) / Object::Num(5.0), Ok(Object::Num(0.8)));
        assert_eq!(Object::Num(4.0).gt(&Object::Num(5.0)), Ok(Object::Bool(false)));
        assert_eq!(Object::Num(4.0).lt(&Object::Num(5.0)), Ok(Object::Bool(true)));
        assert_eq!(Object::Num(4.0).lt(&Object::Num(5.0)), Ok(Object::Bool(true)));
        assert_eq!(Object::Num(4.0).gt(&Object::Num(5.0)), Ok(Object::Bool(false)));
        assert_eq!(Object::Num(4.0).eq(&Object::Num(5.0)), Ok(Object::Bool(false)));
        assert_eq!(Object::Num(4.0).eq(&Object::Num(5.0)), Ok(Object::Bool(false)));
        assert_eq!(Object::Num(4.0).is_truthy(), true);
        assert_eq!(Object::Num(0.0).is_truthy(), true);
        assert_eq!(Object::Bool(true).is_truthy(), true);
        assert_eq!(Object::Bool(false).is_truthy(), false);
        assert_eq!(Object::Nil(()).is_truthy(), false);

    }


}
