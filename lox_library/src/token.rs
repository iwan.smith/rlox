use std::fmt;
use std::fmt::Debug;
use crate::object::Object;

#[allow(dead_code)]
#[derive(Debug,PartialEq,Clone,Copy)]
pub enum TokenType {
    // Single char tokens:
    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,
    Comma,
    Dot,
    Minus,
    Plus,
    Colon,
    Semicolon,
    Slash,
    Star,
    Remainder,
    Question,

    // One or two char tokens
    Bang,
    BangEqual,
    Equal,
    EqualEqual,
    Greater,
    GreaterEqual,
    Less,
    LessEqual,

    // Literals
    Identifier,
    String,
    Number,

    // Keywords
    And,
    Class,
    Else,
    False,
    Fun,
    For,
    If,
    Nil,
    Or,
    Print,
    Return,
    Super,
    This,
    True,
    Var,
    While,
    Break,
    Continue,

    // End of file
    Eof,

    // Null Token that should be ignored
    // Null,
}

impl fmt::Display for TokenType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            // Single char tokens:
            TokenType::LeftParen => write!(f, "("),
            TokenType::RightParen => write!(f, ")"),
            TokenType::LeftBrace => write!(f, "{{"),
            TokenType::RightBrace => write!(f, "}}"),
            TokenType::Comma => write!(f, ","),
            TokenType::Dot => write!(f, "."),
            TokenType::Minus => write!(f, "-"),
            TokenType::Plus => write!(f, "+"),
            TokenType::Colon => write!(f, ":"),
            TokenType::Remainder => write!(f, "%"),
            TokenType::Semicolon => write!(f, ";"),
            TokenType::Slash => write!(f, "/"),
            TokenType::Star => write!(f, "*"),
            TokenType::Question => write!(f, "?"),

            // One or two char tokens
            TokenType::Bang => write!(f, "!"),
            TokenType::BangEqual => write!(f, "!="),
            TokenType::Equal => write!(f, "="),
            TokenType::EqualEqual => write!(f, "=="),
            TokenType::Greater => write!(f, ">"),
            TokenType::GreaterEqual => write!(f, ">="),
            TokenType::Less => write!(f, "<"),
            TokenType::LessEqual => write!(f, "<="),

            // Literals
            TokenType::Identifier => write!(f, "Identifier"),
            TokenType::String => write!(f, "String"),
            TokenType::Number => write!(f, "Number"),

            // Keywords
            TokenType::And => write!(f, "and"),
            TokenType::Class => write!(f, "class"),
            TokenType::Else => write!(f, "else"),
            TokenType::False => write!(f, "false"),
            TokenType::Fun => write!(f, "fun"),
            TokenType::For => write!(f, "for"),
            TokenType::If => write!(f, "if"),
            TokenType::Nil => write!(f, "nil"),
            TokenType::Or => write!(f, "or"),
            TokenType::Print => write!(f, "print"),
            TokenType::Return => write!(f, "return"),
            TokenType::Super => write!(f, "super"),
            TokenType::This => write!(f, "this"),
            TokenType::True => write!(f, "true"),
            TokenType::Var => write!(f, "var"),
            TokenType::While => write!(f, "while"),
            TokenType::Break => write!(f, "break"),
            TokenType::Continue => write!(f, "continue"),

            // End of file
            TokenType::Eof => write!(f, "EOF"),
        }
    }
}

pub fn keyword_map(keyword : &str) -> Option<TokenType>{
    match keyword {
        "and" => Some(TokenType::And),
        "class" => Some(TokenType::Class),
        "else" => Some(TokenType::Else),
        "false" => Some(TokenType::False),
        "for" => Some(TokenType::For),
        "fun" => Some(TokenType::Fun),
        "if" => Some(TokenType::If),
        "nil" => Some(TokenType::Nil),
        "or" => Some(TokenType::Or),
        "print" => Some(TokenType::Print),
        "return" => Some(TokenType::Return),
        "super" => Some(TokenType::Super),
        "this" => Some(TokenType::This),
        "true" => Some(TokenType::True),
        "var" => Some(TokenType::Var),
        "while" => Some(TokenType::While),
        "break" => Some(TokenType::Break),
        "continue" => Some(TokenType::Continue),
        _ => None,
    }

}

#[derive(Debug,PartialEq,Clone)]
pub struct TokenContext <'a> {
    pub lexeme        : &'a str,
    pub byte_count    : usize, // store the line or char number
    pub char_count    : usize, // store the line or char number
    pub source        : &'a str // a reference to the source code
}

#[derive(Debug,PartialEq,Clone)]
pub struct TokenWithContext<'a> {
    pub token_name    : String,
    pub token_type    : TokenType,
    pub representation: Option<Object>,
    pub context       : Option<TokenContext<'a>>
}
impl<'a> TokenWithContext<'a>{

    pub fn expect_context(&'a self, msg: &str) -> &'a TokenContext<'a> {
        self.context.as_ref().expect(msg)
    }
}

// impl Token<'_>{
//     fn to_string(&self) -> String{
//         format!("{:?} {} {} {}", self.token_type, self.lexeme, self.line, self.char_count)
//     }
// }

impl<'a> fmt::Display for TokenWithContext<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {

        match &self.context {
            Some(ctx) => write!(
                f,
                "{:?} {} {}",
                self.token_type, ctx.lexeme, ctx.byte_count
            ),
            None => write!(
                f,
                "{:?}", self.token_type
            ),
        }
    }
}
