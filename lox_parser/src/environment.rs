use std::collections::HashMap;
use std::io::{Error, Write};
use std::mem::swap;
use crate::interpreter_types::LoxObject;
use crate::native_functions;
use crate::statements::FunctionDecl;


pub struct Environment<'buf, 'fundecl>{
    // Scopes begins with the global scope and subsequent
    // block-scopes are added as elements in the vector
    globals    : HashMap<String, LoxObject<'fundecl>>,
    scopes     : Vec<HashMap<String, LoxObject<'fundecl>>>,
    parent_envs: Vec<Vec<HashMap<String, LoxObject<'fundecl>>>>,
    buffer     : &'buf mut dyn Write
}

impl<'buf, 'fundecl> Environment<'_, 'fundecl> {

    pub fn new(buffer: &'buf mut dyn Write) -> Environment {
        let globals     = HashMap::new();
        let scopes      = Vec::new();
        let parent_envs = Vec::new();

        let mut ret = Environment{scopes, globals, parent_envs, buffer};
        native_functions::register_native_functions(&mut ret.globals);

        ret
    }


    pub fn define(&mut self, name: &str, value: LoxObject<'fundecl>){
        if self.scopes.is_empty() {
            self.globals.insert(name.to_string(), value);
        } else {
            self.scopes.last_mut().unwrap().insert(name.to_string(), value);
        };
    }

    pub fn get(&self, name: &str) -> Option<&LoxObject<'fundecl>>{
        self.find_object(name)
    }

    pub fn set(&mut self, name: &str, value: LoxObject<'fundecl>) -> Option<&LoxObject<'fundecl>> {
        match self.find_object_mut(name) {
            Some(var) => {
                *var = value;
                Some(var)
            }
            None => None
        }
    }

    pub fn push_scope(&mut self){
        self.scopes.push(HashMap::new());
    }
    pub fn pop_scope(&mut self){
        assert!(self.scopes.len() >= 1 );
        self.scopes.pop();
    }

    pub fn push_env(&mut self){
        self.parent_envs.push(Vec::new());
        swap(self.parent_envs.last_mut().unwrap(), &mut self.scopes);
        self.push_scope();
    }
    pub fn pop_env(&mut self){
        self.scopes = Vec::new();
        swap(self.parent_envs.last_mut().unwrap(), &mut self.scopes);
        self.parent_envs.pop();
    }

    pub fn print(&mut self, obj: &LoxObject) -> Result<(), Error> {
        writeln!(self.buffer, "{}", obj.to_string() )?;
        Ok(())
    }


    // Search from the newest scope back to the oldest to find 'buf variable by name
    #[must_use]
    fn find_object(&self, name: &str) -> Option<&LoxObject<'fundecl>> {
        for scope in self.scopes.iter().rev() {
            match scope.get(name){
                Some(v) => return Some(v),
                _ => ()
            }
        }
        match self.globals.get(name){
            Some(v) => return Some(v),
            _ => None
        }
    }

    #[must_use]
    fn find_object_mut(&mut self, name: &str) -> Option<&mut LoxObject<'fundecl>> {
        for scope in self.scopes.iter_mut().rev() {
            match scope.get_mut(name){
                Some(v) => return Some(v),
                _ => ()
            }
        }
        match self.globals.get_mut(name){
            Some(v) => return Some(v),
            _ => None
        }
    }
}
