use crate::expressions::{Binary, Assignment, Expression, Group, Literal, Ternary, Unary, Variable, Call};
use lox_library::token::{TokenWithContext, TokenType};
use TokenType as TT;
use crate::expression_tree_builder_errors::*;
use lox_library::lox_error::Error;
use crate::parser::Parser;


// language grammar
//
// Recursive descent starting from lowest priority operators
//
// expression     → assignment;
// assignment     → IDENTIFIER "=" assignment | ternary | logic_or;
// ternary        → logic_or ( "?" expression ":" expression )* ;
// logic_or       → logic_and ( "or" logic_and )* ;
// logic_and      → equality ( "and" equality )* ;
// equality       → comparison ( ( "!=" | "==" ) comparison )* ;
// comparison     → term ( ( ">" | ">=" | "<" | "<=" ) term )* ;
// term           → factor ( ( "-" | "+" ) factor )* ;
// factor         → unary ( ( "/" | "*" | "%" ) unary )* ;
// unary          → ( "!" | "-" ) unary | call ;
// call           → primary ( "(" arguments? ")" )*

// primary        → NUMBER | STRING | "true" | "false" | "nil" | "(" expression ")" | IDENTIFIER;
// arguments      → expression ( "," expression )*


mod helper{
    use crate::expressions::Logical;
    use super::*;
    type MatcherFunc<'a> = fn(&mut Parser<'a>) -> Result<Box<Expression<'a>>, Error>;

    // a generic function for matching
    pub fn recursive_matcher<'a>(p: &mut Parser<'a>,
                                 next_matcher: MatcherFunc<'a>, operator_types : &[TT], is_binary: bool ) -> Result<Box<Expression<'a>>, Error> {

        // Rule     → next_matcher ( [operator_types] next_matcher )* ;

        let mut expr = next_matcher(p)?;

        while p.match_tokens(operator_types){
            let operator = p.previous()?;
            let right = next_matcher(p)?;

            if is_binary {
                expr = Expression::new_binary(Binary{
                    left:expr,
                    operator: operator.clone(),
                    right,
                })
            } else {
                expr = Expression::new_logical(Logical{
                    left:expr,
                    operator: operator.clone(),
                    right,
                })
            }
        }

        Ok(expr)

    }

}

pub fn parse_tokens<'a>(tokens: &'a Vec<TokenWithContext<'a>>) -> Result<Box<Expression<'a>>, Error>{

    let mut p = Parser::new(tokens);
    expression(&mut p)
}

pub fn expression<'a>(p: &mut Parser<'a>) -> Result<Box<Expression<'a>>, Error>  {
    // expression     → equality ;
    assignment(p)
}

pub fn assignment<'a>(p: &mut Parser<'a>) -> Result<Box<Expression<'a>>, Error>  {
    // assignment     → IDENTIFIER "=" assignment | ternary ;

    let expr = ternary(p)?;

    if p.match_token(TT::Equal) {
        let equals = p.previous()?;
        let value = assignment(p)?;

        return match expr.as_ref() {
            Expression::Variable(v) => {
                Ok(Expression::new_assignment(Assignment { name: v.name.clone(), value }))
            }
            _ => {
                let ctx = equals.context.as_ref().unwrap();
                let loc_char = ctx.char_count;
                Err(Error { loc_char, error_string: "expression to the left of assignment is not a variable name".to_string() })
            }
        }
    }

    Ok(expr)
}

fn ternary<'a>(p: &mut Parser<'a>) -> Result<Box<Expression<'a>>, Error> {
    // ternary        → logic_or ( "?" expression ":" expression )*
    let left = logic_or(p)?;

    if ! p.match_token(TT::Question) {
        return Ok(left);
    }

    let middle = expression(p)?;

    if ! p.match_token(TT::Colon) {
        let tok = p.previous()?;
        let ctx = tok.context.as_ref().unwrap();
        let error_string = "No ':' in ternary operator".to_string();
        return Err(Error {loc_char:ctx.char_count, error_string});
    }
    let right = expression(p)?;

    Ok(Box::new(Expression::Ternary(Ternary{left,middle,right})))

}

fn logic_or<'a>(p: &mut Parser<'a>) -> Result<Box<Expression<'a>>, Error>  {
    // logic_or       → logic_and ( "or" logic_and )* ;
    helper::recursive_matcher(p, logic_and, &[TT::Or], false)
}
fn logic_and<'a>(p: &mut Parser<'a>) -> Result<Box<Expression<'a>>, Error>  {
    // logic_and      → equality ( "and" equality )* ;
    helper::recursive_matcher(p, equality, &[TT::And], false)
}

fn equality<'a>(p: &mut Parser<'a>) -> Result<Box<Expression<'a>>, Error>  {
    // equality       → comparison ( ( "!=" | "==" ) comparison )* ;
    helper::recursive_matcher(p, comparison, &[TT::BangEqual, TT::EqualEqual], true)
}

fn comparison<'a>(p: &mut Parser<'a>) -> Result<Box<Expression<'a>>, Error>  {
    // comparison     → term ( ( ">" | ">=" | "<" | "<=" ) term )* ;
    helper::recursive_matcher(p, term, &[TT::Greater, TT::GreaterEqual, TT::Less, TT::LessEqual], true)
}

fn term<'a>(p: &mut Parser<'a>) -> Result<Box<Expression<'a>>, Error>  {
    // term           → factor ( ( "-" | "+" ) factor )* ;
    helper::recursive_matcher(p, factor, &[TT::Plus, TT::Minus], true)
}

fn factor<'a>(p: &mut Parser<'a>) -> Result<Box<Expression<'a>>, Error>  {
    // factor         → unary ( ( "/" | "*" ) unary )* ;
    helper::recursive_matcher(p, unary, &[TT::Star, TT::Slash, TT::Remainder], true)
}

fn unary<'a>(p: &mut Parser<'a>) -> Result<Box<Expression<'a>>, Error>  {
    // unary          → ( "!" | "-" ) unary | primary ;
    if p.match_tokens(&[TT::Bang, TT::Minus]){
        let operator = p.previous()?;
        let right = unary(p)?;

        let expr = Expression::new_unary(Unary{
            operator: operator.clone(),
            right,
        });
        return Ok(expr);
    }
    call(p)
}

fn call<'a>(p: &mut Parser<'a>) -> Result<Box<Expression<'a>>, Error> {

    let mut expr = primary(p)?;

    loop {
        if p.match_token(TT::LeftParen) {
            expr = finish_call(p, expr)?;
        } else {
            break;
        }
    }

    Ok(expr)
}

fn finish_call<'a>(p: &mut Parser<'a>, expr: Box<Expression<'a>>) -> Result<Box<Expression<'a>>, Error> {

    let mut arguments = Vec::new();

    if let Ok(TokenWithContext{token_type:TT::RightParen, ..}) = p.peek_next() {} else{
        loop {
            arguments.push(expression(p)?);

            if arguments.len() == 255 {
                let loc_char = p.peek_next().unwrap().context.as_ref().unwrap().char_count;
                let error_string = "Expected closing ')' after arguments in function call".to_string();
                return Err(Error{loc_char, error_string});
            }

            if !p.match_token(TT::Comma) {
                break;
            }

        }
    }
    if !p.match_token(TT::RightParen){
        let loc_char = p.peek_next().unwrap().context.as_ref().unwrap().char_count;
        let error_string = "Expected closing ')' after arguments in function call".to_string();
        return Err(Error{loc_char, error_string});
    }

    Ok(Expression::new_call(Call{callee:expr, arguments}))
}


fn primary<'a>(p: &mut Parser<'a>) -> Result<Box<Expression<'a>>, Error> {
    // primary        → NUMBER | STRING | "true" | "false" | "nil"  | "(" expression ")" ;

    // match literal values
    if p.match_tokens(&[TT::Number, TT::String, TT::True, TT::False, TT::Nil]){
        let t = p.previous()?;
        return Ok(Expression::new_literal(Literal{expr:t.clone()}))
    }

    if p.match_token(TT::LeftParen) {
        let expr = expression(p)?;
        if !p.match_token(TT::RightParen) {
            let tok = p.previous()?;
            let ctx = tok.context.as_ref().unwrap();
            let error_string = "No closing parenthesis after expression".to_string();
            return Err(Error {loc_char:ctx.char_count, error_string});
        }
        return Ok(Expression::new_group(Group{expr}));
    }

    if p.match_token(TT::Identifier){
        let t = p.previous()?;
        return Ok(Expression::new_variable(Variable{name: t.clone()}));
    }


    // Error Handling

    let tok = p.peek_next()?;

    // Tried to use binary operator in a unary context:
    const BINARY_TOKENS: [TokenType; 11] = [TT::Star, TT::Minus, TT::Slash, TT::Plus, TT::LessEqual, TT::Less, TT::GreaterEqual, TT::Greater, TT::EqualEqual, TT::BangEqual, TT::Equal];
    if BINARY_TOKENS.contains(&tok.token_type) {
        let ctx = tok.context.as_ref().unwrap();
        let error_string = used_binary_as_unary(tok, tok);
        return Err(Error {loc_char:ctx.char_count, error_string});
    }


    let ctx = tok.context.as_ref().unwrap();
    let error_string = "Expected expression".to_string();
    Err(Error {loc_char:ctx.char_count, error_string})
}
