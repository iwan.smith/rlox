use lox_library::token::TokenWithContext;
use lox_library::lox_error::format_line_from_token;

pub fn used_binary_as_unary(t1: &TokenWithContext, _: &TokenWithContext) -> String {

    let desc = format!("Binary operator '{}' incorrectly used in a unary context: ", t1.token_name);
    let source = format_line_from_token(t1);
    format!("\n{}\n{}\n", desc, source)
}