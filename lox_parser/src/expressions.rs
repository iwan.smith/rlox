use lox_library::token::TokenWithContext;

// Visitors will need to implement this trait
pub trait VisitorExpr<T>{
    fn visit_assignment (&mut self , b: &Assignment) -> T;
    fn visit_ternary    (&mut self , b: &Ternary   ) -> T;
    fn visit_binary     (&mut self , b: &Binary    ) -> T;
    fn visit_call       (&mut self , b: &Call      ) -> T;
    fn visit_grouping   (&mut self , b: &Group     ) -> T;
    fn visit_literal    (&mut self , b: &Literal   ) -> T;
    fn visit_logical    (&mut self , b: &Logical   ) -> T;
    fn visit_unary      (&mut self , b: &Unary     ) -> T;
    fn visit_variable   (&mut self , b: &Variable  ) -> T;
}

pub enum Expression<'a> {
    Assignment(Assignment<'a>),
    Ternary   (Ternary   <'a>),
    Binary    (Binary    <'a>),
    Call      (Call      <'a>),
    Group     (Group     <'a>),
    Literal   (Literal   <'a>),
    Logical   (Logical   <'a>),
    Unary     (Unary     <'a>),
    Variable  (Variable  <'a>)
}

impl Expression<'_> {

    pub fn new_assignment(e: Assignment) -> Box<Expression> { Box::new(Expression::Assignment(e) )}
    pub fn new_ternary   (e: Ternary   ) -> Box<Expression> { Box::new(Expression::Ternary   (e) )}
    pub fn new_binary    (e: Binary    ) -> Box<Expression> { Box::new(Expression::Binary    (e) )}
    pub fn new_call      (e: Call      ) -> Box<Expression> { Box::new(Expression::Call      (e) )}
    pub fn new_group     (e: Group     ) -> Box<Expression> { Box::new(Expression::Group     (e) )}
    pub fn new_literal   (e: Literal   ) -> Box<Expression> { Box::new(Expression::Literal   (e) )}
    pub fn new_logical   (e: Logical   ) -> Box<Expression> { Box::new(Expression::Logical   (e) )}
    pub fn new_unary     (e: Unary     ) -> Box<Expression> { Box::new(Expression::Unary     (e) )}
    pub fn new_variable  (e: Variable  ) -> Box<Expression> { Box::new(Expression::Variable  (e) )}

    pub fn accept<T>(&self , v: &mut dyn VisitorExpr<T> ) -> T{
        match self{
            Expression::Assignment(e)=> v.visit_assignment(e),
            Expression::Ternary(e)   => v.visit_ternary   (e),
            Expression::Binary(e)    => v.visit_binary    (e),
            Expression::Call(e)      => v.visit_call      (e),
            Expression::Group(e)     => v.visit_grouping  (e),
            Expression::Literal(e)   => v.visit_literal   (e),
            Expression::Logical(e)   => v.visit_logical   (e),
            Expression::Unary(e)     => v.visit_unary     (e),
            Expression::Variable(e)  => v.visit_variable  (e),
        }
    }
}

pub struct Assignment<'a> {
    pub name  : TokenWithContext<'a>,
    pub value : Box<Expression<'a>>,
}

pub struct Ternary<'a> {
    pub left   : Box<Expression<'a>>,
    pub middle : Box<Expression<'a>>,
    pub right  : Box<Expression<'a>>,
}

pub struct Binary<'a> {
    pub left     : Box<Expression<'a>>,
    pub operator : TokenWithContext<'a>,
    pub right    : Box<Expression<'a>>,
}

pub struct Call<'a> {
    pub callee    : Box<Expression<'a>>,
    pub arguments : Vec<Box<Expression<'a>>>,
}
pub struct Group<'a> {
    pub expr: Box<Expression<'a>>
}

pub struct Literal<'a> {
    pub expr: TokenWithContext<'a>
}

pub struct Logical<'a> {
    pub left     : Box<Expression<'a>>,
    pub operator : TokenWithContext<'a>,
    pub right    : Box<Expression<'a>>,
}

pub struct Unary<'a> {
    pub operator : TokenWithContext<'a>,
    pub right    : Box<Expression<'a>>,
}

pub struct Variable<'a> {
    pub name : TokenWithContext<'a>,
}
