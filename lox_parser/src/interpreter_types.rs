use std::fmt::{Debug, Formatter};
use lox_library::Object;
use crate::statements::FunctionDecl;

type FunctionNativePtr = fn(Vec<LoxObject>) -> Result<LoxObject,String>;


#[derive(Clone)]
pub struct LoxFunctionNative {
    pub arity : usize,
    function  : FunctionNativePtr
}

impl PartialEq for LoxFunctionNative {
    fn eq(&self, other: &Self) -> bool {
        self.function as usize == other.function as usize
    }
}

impl Debug for LoxFunctionNative {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str("Native Function")
    }
}


impl LoxFunctionNative {
    pub fn call(self, args:Vec<LoxObject>) -> Result<LoxObject, String> {

        if args.len() != self.arity {
            return Err("Function called with the wrong number of arguments".to_string());
        }

        (self.function)(args)
    }
}

#[derive(Clone)]
pub struct LoxFunctionDeclared<'fundecl> {
    pub arity : usize,
    pub function: &'fundecl FunctionDecl<'fundecl>
}

impl PartialEq for LoxFunctionDeclared<'_> {
    fn eq(&self, other: &Self) -> bool {
        self.function.name == other.function.name
    }

    fn ne(&self, other: &Self) -> bool {
        self.function.name != other.function.name
    }
}

impl Debug for LoxFunctionDeclared<'_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("LoxFunctionDeclared")
            .field("Name", self.function.name)
            .finish()
    }
}

#[derive(Debug,PartialEq,Clone)]
pub enum LoxObject<'fundecl>{
    Object(Object),
    FunctionNative(LoxFunctionNative),
    FunctionDeclared(LoxFunctionDeclared<'fundecl>)
}


impl LoxObject<'_> {

    pub fn new_str (str: String) -> LoxObject<'static> { LoxObject::Object(Object::Str (str)) }
    pub fn new_num (num: f64   ) -> LoxObject<'static> { LoxObject::Object(Object::Num (num)) }
    pub fn new_bool(b  : bool  ) -> LoxObject<'static> { LoxObject::Object(Object::Bool(b  )) }
    pub fn new_nil (           ) -> LoxObject<'static> { LoxObject::Object(Object::Nil (() )) }

    pub fn new_native_function  (function: FunctionNativePtr, arity: usize) -> LoxObject<'static> {
        LoxObject::FunctionNative( LoxFunctionNative {function, arity})
    }

    pub fn new_declared_function<'fundecl>(function: &'fundecl FunctionDecl<'fundecl>, arity: usize) -> LoxObject<'fundecl> {
        LoxObject::FunctionDeclared( LoxFunctionDeclared{function, arity})
    }


    pub fn is_truthy(&self) -> bool {
        match self{
            LoxObject::Object(obj) => obj.is_truthy(),
            LoxObject::FunctionNative(_) => true,
            LoxObject::FunctionDeclared(_) => true,
        }
    }


    pub fn to_string(&self) -> String {
        match self{
            LoxObject::Object(obj) => obj.to_string(),
            LoxObject::FunctionDeclared(_) => todo!(),
            LoxObject::FunctionNative(_) => todo!(),
        }
    }

}
