use std::collections::HashMap;
use std::time::{Duration, SystemTime, UNIX_EPOCH};
use lox_library::Object;
use crate::interpreter_types::LoxObject;


fn clock(_ : Vec<LoxObject> ) -> Result<LoxObject, String> {
    let time_since_epoch = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("Current time should be after the Epoch");
    let secs = time_since_epoch.as_micros() as f64;

    Ok( LoxObject::new_num(secs/1000000.0) )
}

fn sleep(args : Vec<LoxObject> ) -> Result<LoxObject, String> {

    match args[0] {
        LoxObject::Object(Object::Num(n)) => std::thread::sleep(Duration::from_micros((n*1_000_000.0) as u64)),
        LoxObject::Object(_)   => return Err( "Argument to native functon 'sleep' must be a number".to_string() ),
        LoxObject::FunctionDeclared(_) => return Err( "Argument to native functon 'sleep' must be a number".to_string() ),
        LoxObject::FunctionNative(_)   => return Err( "Argument to native functon 'sleep' must be a number".to_string() )
    };


    Ok(LoxObject::new_nil())
}

fn get_42(_ : Vec<LoxObject> ) -> Result<LoxObject, String> {
    Ok( LoxObject::new_num(42.0) )
}


pub fn register_native_functions(funcs: &mut HashMap<String, LoxObject>){

    funcs.insert("clock".to_string(),  LoxObject::new_native_function(clock, 0));
    funcs.insert("sleep".to_string(),  LoxObject::new_native_function(sleep, 1));
    funcs.insert("get_42".to_string(), LoxObject::new_native_function(get_42, 0));

}