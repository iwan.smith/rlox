use lox_library::token::{TokenWithContext, TokenType};
use TokenType as TT;
use lox_library::lox_error::Error;
use crate::statements::FunctionDecl;


pub struct Parser<'a>  {
    pub tokens: &'a Vec<TokenWithContext<'a>>,
    pub current: usize,

    // some tokens, e.g. break are only valid
    // within a while/for loop. This variable
    // keeps track of the level of while/for
    // loop nesting. the break statement can
    // only be created if loop_nesting_level > 0
    pub loop_nesting_level: usize,

    fun_decls: Vec<&'a FunctionDecl<'a>>,

}

impl<'a> Parser<'a> {

    pub fn new(tokens: &'a Vec<TokenWithContext<'a>>) ->Parser<'a> {
        Parser{tokens, current: 0, loop_nesting_level: 0, fun_decls: Vec::new()}
    }

    pub fn is_at_end(&self) -> bool {
        return self.current >= self.tokens.len() || self.tokens[self.current].token_type == TokenType::Eof;
    }

    pub fn peek_next(&self) -> Result<&'a TokenWithContext<'a>, Error> {
        if self.is_at_end() {
            let tok = &self.tokens[self.current];
            let ctx = &tok.context.as_ref().unwrap();

            let error_string = "Unexpectedly reached end of token sequence".to_string();
            return Err(Error { loc_char: ctx.char_count, error_string })
        }
        return Ok(&self.tokens[self.current])
    }

    pub fn advance(&mut self) -> Result<&'a TokenWithContext<'a>, Error> {
        let ret = self.peek_next()?;
        self.current += 1;
        Ok(ret)
    }

    pub fn previous(&self) -> Result<&'a TokenWithContext<'a>, Error> {
        if self.current == 0 {
            let tok = &self.tokens[self.current];
            let ctx = &tok.context.as_ref().unwrap();

            let error_string = "Tried to read a token before advancing to first token".to_string();
            return Err(Error { loc_char: ctx.char_count, error_string })
        }
        return Ok(&self.tokens[self.current - 1])
    }


    #[allow(dead_code)]
    pub fn match_tokens(&mut self, tokens_to_match: &[TT]) -> bool {
        match self.peek_next() {
            Err(_) => { return false }
            Ok(cur_token) => {
                if tokens_to_match.contains(&cur_token.token_type) {
                    self.current += 1;
                    return true;
                }
            }
        }
        return false;
    }

    pub fn match_token(&mut self, token_to_match: TT) -> bool {
        self.match_tokens(&[token_to_match])
    }

    pub fn synchronize(&mut self) {
        let _ = self.advance();

        while !self.is_at_end() {
            if let Ok(TokenWithContext{token_type:TT::Semicolon, .. }) = self.previous(){
                return;
            }
            let boundary_tokens = [
                TT::Semicolon,
                TT::Class,
                TT::Fun,
                TT::Var,
                TT::For,
                TT::If,
                TT::While,
                TT::Print,
                TT::Return];
            let next_tokentype = self.peek_next().unwrap().token_type;

            if boundary_tokens.contains(&next_tokentype) {
                return;
            }
            let _ = self.advance();
        }
    }
}
