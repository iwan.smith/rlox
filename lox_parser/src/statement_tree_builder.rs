use crate::expression_tree_builder;
use crate::expression_tree_builder::expression;
use crate::expressions::{Expression, Literal};
use crate::parser::Parser;
use crate::statements;
use crate::statements::{Block, Expr, Statement};
use lox_library::lox_error::Error;
use lox_library::Object;
use lox_library::token::{TokenType, TokenWithContext};
use TokenType as TT;

// language grammar
//
// program        → declaration* EOF ;
//
// declaration    → emptyStmnt
//                | forStmnt
//                | ifStmt
//                | varDecl
//                | funDecl
//                | exprStmt
//                | printStmt
//                | returnStmt
//                | whileStmnt
//                | breakStmnt
//                | continueStmnt
//                | blockStmnt ;
//
// emptyStmnt     → ";"
// forStmnt       → "for" "(" ( varDecl | exprStmt | ";" ) expression? ";" expression? ")" statement ;
// ifStmt         → "if" "(" expression ")" statement ( "else" statement )? ;
// varDecl        → "var" IDENTIFIER ( "=" expression )? ";" ;
// funDecl        → "fun" function ;
// exprStmt       → expression ";" ;
// printStmt      → "print" expression ";" ;
// returnStmt     → "return" expression? ";" ;
// whileStmt      → "while" "(" expression ")" statement ;
// breakStmt      → "break" ";" ;
// continueStmt   → "continue" ";" ;
// blockStmnt     → "{" declaration* "}" ;
//
// function       → IDENTIFIER "(" (parameters) ")" block;
// parameters     → IDENTIFIER ( "," IDENTIFIER )* ;


pub fn parse_tokens<'a>(p: &mut Parser<'a>) -> Result<Vec<Box<Statement<'a>>>, Vec<Error>> {
    let mut ret = Vec::new();
    let mut err = Vec::new();
    while !p.is_at_end() {
        match parse_declaration(p) {
            Ok(r) => ret.push(r),
            Err(e) => err.push(e),
        }
    }
    if err.is_empty() {
        Ok(ret)
    } else {
        Err(err)
    }
}

fn parse_declaration<'a>(p: &mut Parser<'a>) -> Result<Box<Statement<'a>>, Error> {
    let ret = match p.peek_next()?.token_type {
        TT::Semicolon => { p.advance()?;  empty_statement   (p) }
        TT::Fun       => { p.advance()?;  fundecl_statement (p) }
        TT::Var       => { p.advance()?;  vardecl_statement (p) }
        TT::For       => { p.advance()?;  for_statement     (p) }
        TT::While     => { p.advance()?;  while_statement   (p) }
        TT::Break     => { p.advance()?;  break_statement   (p) }
        TT::Continue  => { p.advance()?;  continue_statement(p) }
        TT::Print     => { p.advance()?;  print_statement   (p) }
        TT::Return    => { p.advance()?;  return_statement  (p) }
        TT::LeftBrace => { p.advance()?;  block_statement   (p) }
        TT::If        => { p.advance()?;  if_statement      (p) }
        _             => expression_statement(p)
    };

    if ret.is_err() {
        p.synchronize();
    }

    ret
}

fn verify_semicolon(p: &mut Parser) -> Result<(), Error> {
    if !p.match_token(TT::Semicolon) {
        let tok = p.previous()?;
        let ctx = tok.expect_context("Token should have context");
        let error_string = "No semicolon after expression".to_string();
        return Err(Error {
            loc_char: ctx.char_count,
            error_string,
        });
    }
    Ok(())
}

fn empty_statement<'a>(_: &mut Parser<'a>) -> Result<Box<Statement<'a>>, Error> {
    Ok(Box::new(Statement::Empty(statements::Empty {})))
}

fn vardecl_statement<'a>(p: &mut Parser<'a>) -> Result<Box<Statement<'a>>, Error> {
    let prev = p.previous()?;

    if !p.match_token(TT::Identifier) {
        let ctx = prev.expect_context("Token should have context");
        return Err(Error {
            loc_char: ctx.char_count,
            error_string: "Expected Identifier in variable declaration".to_string(),
        });
    }

    let var_name = p.previous()?;

    let mut initializer: Option<Box<Expression<'a>>> = None;

    if p.match_token(TT::Equal) {
        initializer = Some(expression_tree_builder::expression(p)?);
    }

    verify_semicolon(p)?;

    Ok(Statement::new_var_decl(statements::VarDecl {
        name: var_name,
        expr: initializer,
    }))
}

fn fundecl_statement<'a>(p: &mut Parser<'a>) -> Result<Box<Statement<'a>>, Error> {

    if !p.match_token(TT::Identifier) {
        let ctx = p.previous()?.expect_context("Token should have context");
        return Err(Error {
            loc_char: ctx.char_count,
            error_string: "Expected Identifier in function declaration".to_string(),
        });
    }

    let name = p.previous()?;

    if !p.match_token(TT::LeftParen){
        let ctx = p.previous()?.expect_context("Token should have context");
        return Err(Error {
            loc_char: ctx.char_count,
            error_string: "Expected '(' after function name".to_string(),
        });
    }

    let mut params = Vec::new();
    if !p.match_token(TT::RightParen) {

        loop {
            let identifier = p.advance()?;
            if identifier.token_type != TT::Identifier {
                let ctx = p.previous()?.expect_context("Token should have context");
                return Err(Error {
                    loc_char: ctx.char_count,
                    error_string: "Expected Identifier in function parameter list".to_string(),
                });
            }
            params.push(identifier);
            if !p.match_token(TT::Comma) {
                break;
            }
        }

        if !p.match_token(TT::RightParen) {
            let ctx = p.previous()?.expect_context("Token should have context");
            return Err(Error {
                loc_char: ctx.char_count,
                error_string: "Expected ')' after function parameters".to_string(),
            });
        }

    }

    if !p.match_token(TT::LeftBrace) {
        let ctx = p.previous()?.expect_context("Token should have context");
        return Err(Error {
            loc_char: ctx.char_count,
            error_string: "Expect { to indicate function body".to_string(),
        });
    }
    let body = block_statement(p)?;

    Ok(Statement::new_function(statements::FunctionDecl {name, params, body}))
}


fn for_statement<'a>(p: &mut Parser<'a>) -> Result<Box<Statement<'a>>, Error> {
    // This function parses a for loop and de-sugars it into a while loop

    let prev = p.previous()?;

    // verify that the a paren follows "while"
    if !p.match_token(TT::LeftParen) {
        let ctx = prev.expect_context("Token should have context");
        return Err(Error {
            loc_char: ctx.char_count,
            error_string: "Expected '(' after 'while'')'".to_string(),
        });
    }

    // Create the initializer for the while loop
    let initializer = if p.match_token(TT::Semicolon) {
        empty_statement(p)?
    } else if p.match_token(TT::Var) {
        vardecl_statement(p)?
    } else {
        expression_statement(p)?
    };

    // Create the condition for the while loop
    let mut condition = if let TokenWithContext{token_type:TT::Semicolon, .. } = p.peek_next()? {
        None
    } else {
        Some(expression(p)?)
    };

    verify_semicolon(p)?;

    // Create the iteration expression for the while loop
    let increment =  if let TokenWithContext{token_type:TT::RightParen, .. } = p.peek_next()? {
        None
    } else {
        Some(expression(p)?)
    };

    // verify the closing paren for the while loop
    if !p.match_token(TT::RightParen) {
        let ctx = prev.expect_context("Token should have context");
        return Err(Error {
            loc_char: ctx.char_count,
            error_string: "Expected ')' after clauses ')'".to_string(),
        });
    }

    // Parse the body of the while loop
    p.loop_nesting_level += 1;
    let mut  body = parse_declaration(p)?;
    assert!(p.loop_nesting_level > 0);
    p.loop_nesting_level -= 1;

    //
    // Start constructing a while loop from the for loop
    //

    // Add the iteration expression to the body of the loop and run before the for body
    if increment.is_some() {
        let incr = increment.unwrap();
        let increment_statement = Statement::new_expr(Expr{expr:incr});
        body = Statement::new_block(Block{stmnts:vec![body, increment_statement]});
    }

    // If there is no condition, set the condition to a true literal
    if condition.is_none() {
        let true_cond = TokenWithContext{ token_name: "true".to_string(), representation:Some(Object::Bool(true)), token_type:TT::True, context:None };
        condition = Some(Box::new(Expression::Literal(Literal{ expr:true_cond })))
    }

    // Create the while loop
    let while_loop =  Statement::new_while(statements::While {
        condition: condition.unwrap(),
        body,
    });

    // If there is an initializer, then create a new block
    // with the initializer and the while loop
    // otherwise return the while loop
    if let Statement::Empty(..) = *initializer {
        Ok(while_loop)
    } else {
        let init_and_while = vec![initializer, while_loop];
        Ok(Statement::new_block(Block{stmnts:init_and_while}))
    }
}

fn while_statement<'a>(p: &mut Parser<'a>) -> Result<Box<Statement<'a>>, Error> {
    let prev = p.previous()?;

    if !p.match_token(TT::LeftParen) {
        let ctx = &prev.expect_context("Token should have context");
        return Err(Error {
            loc_char: ctx.char_count,
            error_string: "Expected '(' after 'while'')'".to_string(),
        });
    }

    let condition = expression_tree_builder::expression(p)?;

    if !p.match_token(TT::RightParen) {
        let ctx = &prev.expect_context("Token should have context");
        return Err(Error {
            loc_char: ctx.char_count,
            error_string: "Expected ')' after condition ')'".to_string(),
        });
    }

    p.loop_nesting_level += 1;
    let body = parse_declaration(p)?;
    assert!(p.loop_nesting_level > 0);
    p.loop_nesting_level -= 1;

    Ok(Statement::new_while(statements::While {
        condition,
        body,
    }))
}

fn break_statement<'a>(p: &mut Parser<'a>) -> Result<Box<Statement<'a>>, Error> {

    if p.loop_nesting_level == 0 {
        let prev = p.previous()?;
        let ctx = &prev.expect_context("Token should have context");
        return Err(Error {
            loc_char: ctx.char_count,
            error_string: "break can only be used within a loop".to_string(),
        });
    }

    verify_semicolon(p)?;
    Ok(Statement::new_break())
}

fn continue_statement<'a>(p: &mut Parser<'a>) -> Result<Box<Statement<'a>>, Error> {

    if p.loop_nesting_level == 0 {
        let prev = p.previous()?;
        let ctx = &prev.expect_context("Token should have context");
        return Err(Error {
            loc_char: ctx.char_count,
            error_string: "continue can only be used within a loop".to_string(),
        });
    }

    verify_semicolon(p)?;
    Ok(Statement::new_continue())
}

fn print_statement<'a>(p: &mut Parser<'a>) -> Result<Box<Statement<'a>>, Error> {
    let expr = expression_tree_builder::expression(p)?;

    verify_semicolon(p)?;

    Ok(Statement::new_print(statements::Print { expr }))
}

fn return_statement<'a>(p: &mut Parser<'a>) -> Result<Box<Statement<'a>>, Error> {

    let expr = if let TokenWithContext{token_type:TT::Semicolon, .. } = p.peek_next()? {
        let expr = TokenWithContext{token_name : "nil".to_string(), token_type:TokenType::Nil, representation:Some(Object::Nil(())), context: None };
        Expression::new_literal(Literal{expr})
    } else {
        expression_tree_builder::expression(p)?
    };

    verify_semicolon(p)?;

    Ok(Statement::new_return(statements::Return{expr}))
}

fn expression_statement<'a>(p: &mut Parser<'a>) -> Result<Box<Statement<'a>>, Error> {
    let expr = expression_tree_builder::expression(p)?;

    verify_semicolon(p)?;

    Ok(Statement::new_expr(statements::Expr { expr }))
}

fn block_statement<'a>(p: &mut Parser<'a>) -> Result<Box<Statement<'a>>, Error> {
    let mut stmnts = Vec::new();

    while p.peek_next()?.token_type != TT::RightBrace {
        stmnts.push(parse_declaration(p)?);
    }

    assert_eq!(p.advance()?.token_type, TT::RightBrace);

    Ok(Statement::new_block(statements::Block { stmnts }))
}

fn if_statement<'a>(p: &mut Parser<'a>) -> Result<Box<Statement<'a>>, Error> {
    let lp = p.advance()?;
    if lp.token_type != TT::LeftParen {
        let ctx = &lp.expect_context("Token should have context");
        return Err(Error {
            loc_char: ctx.char_count,
            error_string: "Expect parentheses after 'if'".to_string(),
        });
    }

    let condition = expression_tree_builder::expression(p)?;

    let rp = p.advance()?;
    if rp.token_type != TT::RightParen {
        let ctx = &lp.expect_context("Token should have context");
        return Err(Error {
            loc_char: ctx.char_count,
            error_string: "Expect closing parenthesis after expression in 'if' clause.".to_string(),
        });
    }

    let then_branch = parse_declaration(p)?;
    let else_branch = if p.match_token(TT::Else) {
        Some(parse_declaration(p)?)
    } else {
        None
    };

    Ok(Statement::new_if(statements::If {
        condition,
        then_branch,
        else_branch,
    }))
}
