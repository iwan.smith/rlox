use lox_library::token::TokenWithContext;
use crate::expressions::Expression;

// Visitors will need to implement this trait
pub trait VisitorStmnt<'a, T>{
    fn visit_empty        (&mut self, b: &'a Empty       ) -> T;
    fn visit_if           (&mut self, b: &'a If          ) -> T;
    fn visit_vardecl      (&mut self, b: &'a VarDecl     ) -> T;
    fn visit_expr         (&mut self, b: &'a Expr        ) -> T;
    fn visit_print        (&mut self, b: &'a Print       ) -> T;
    fn visit_return       (&mut self, b: &'a Return      ) -> T;
    fn visit_while        (&mut self, b: &'a While       ) -> T;
    fn visit_break        (&mut self, b: &'a Break       ) -> T;
    fn visit_continue     (&mut self, b: &'a Continue    ) -> T;
    fn visit_block        (&mut self, b: &'a Block       ) -> T;
    fn visit_function_decl(&mut self, b: &'a FunctionDecl) -> T;
}


pub enum Statement<'a> {
    Empty   (Empty       ),
    If      (If      <'a>),
    VarDecl (VarDecl <'a>),
    Expr    (Expr    <'a>),
    Print   (Print   <'a>),
    Return  (Return  <'a>),
    While   (While   <'a>),
    Break   (Break       ),
    Continue(Continue    ),
    Block   (Block   <'a>),
    Function(FunctionDecl<'a>),
}

impl<'a> Statement<'a> {

    pub fn new_empty   (               ) -> Box<Statement<'static>> { Box::new(Statement::Empty   ( Empty{}   ))}
    pub fn new_if      (s: If          ) -> Box<Statement>          { Box::new(Statement::If      ( s         ))}
    pub fn new_var_decl(s: VarDecl     ) -> Box<Statement>          { Box::new(Statement::VarDecl ( s         ))}
    pub fn new_expr    (s: Expr        ) -> Box<Statement>          { Box::new(Statement::Expr    ( s         ))}
    pub fn new_print   (s: Print       ) -> Box<Statement>          { Box::new(Statement::Print   ( s         ))}
    pub fn new_return  (s: Return      ) -> Box<Statement>          { Box::new(Statement::Return  ( s         ))}
    pub fn new_while   (s: While       ) -> Box<Statement>          { Box::new(Statement::While   ( s         ))}
    pub fn new_break   (               ) -> Box<Statement<'static>> { Box::new(Statement::Break   ( Break{}   ))}
    pub fn new_continue(               ) -> Box<Statement<'static>> { Box::new(Statement::Continue( Continue{}))}
    pub fn new_block   (s: Block       ) -> Box<Statement>          { Box::new(Statement::Block   ( s         ))}
    pub fn new_function(s: FunctionDecl) -> Box<Statement>          { Box::new(Statement::Function( s         ))}



    pub fn accept<T>(&'a self, v: &mut dyn VisitorStmnt<'a, T>) -> T {
        match self{
            Statement::Empty   (e) => v.visit_empty        (e),
            Statement::If      (e) => v.visit_if           (e),
            Statement::VarDecl (e) => v.visit_vardecl      (e),
            Statement::Expr    (e) => v.visit_expr         (e),
            Statement::Print   (e) => v.visit_print        (e),
            Statement::Return  (e) => v.visit_return       (e),
            Statement::While   (e) => v.visit_while        (e),
            Statement::Break   (e) => v.visit_break        (e),
            Statement::Continue(e) => v.visit_continue     (e),
            Statement::Block   (e) => v.visit_block        (e),
            Statement::Function(e) => v.visit_function_decl(e),
        }
    }
}

pub struct Empty{
}

pub struct If<'a>{
    pub condition:   Box<Expression<'a>>,
    pub then_branch: Box<Statement<'a>>,
    pub else_branch: Option<Box<Statement<'a>>>
}

pub struct VarDecl<'a>{
    pub name: &'a TokenWithContext<'a>,
    pub expr: Option<Box<Expression<'a>>>
}

pub struct Expr<'a>{
    pub expr: Box<Expression<'a>>
}

pub struct Print<'a>{
    pub expr: Box<Expression<'a>>
}

pub struct Return<'a>{
    pub expr: Box<Expression<'a>>
}

pub struct While<'a>{
    pub condition:  Box<Expression<'a>>,
    pub body:       Box<Statement <'a>>
}

pub struct Break{
}

pub struct Continue{
}

pub struct Block<'a>{
    pub stmnts: Vec<Box<Statement<'a>>>,
}

pub struct FunctionDecl<'a>{
    pub name  : &'a TokenWithContext<'a>,
    pub params: Vec<&'a TokenWithContext<'a>>,
    pub body  : Box<Statement<'a>>
}

