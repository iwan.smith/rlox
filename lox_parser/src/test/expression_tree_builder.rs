#[cfg(test)]
mod tests {
    use crate::visitor_pretty_printer::*;
    use lox_lexer::lexer::scan_tokens;
    use crate::expression_tree_builder;

    fn parse_and_print_ast(source: &str) -> String{
        let tokens_scanned = scan_tokens(source).unwrap();
        let expr = expression_tree_builder::parse_tokens(&tokens_scanned).unwrap();
        expr.accept(&mut PrettyPrinter::new())
    }

    #[test]
    fn test_expr_parser_primary() {
        assert_eq!(parse_and_print_ast(" true"), "true");
        assert_eq!(parse_and_print_ast(" false"), "false");
        assert_eq!(parse_and_print_ast(" 4.56"), "4.56");
        assert_eq!(parse_and_print_ast(" 004.5600"), "4.56");
        assert_eq!(parse_and_print_ast(r#"  "Hello World"  "#), r#""Hello World""#);
        assert_eq!(parse_and_print_ast("  nil  "), "nil");
    }

    #[test]
    fn test_expr_parser_ternary() {
        assert_eq!(parse_and_print_ast(" true  ?  4  :  5 "), "( true ? 4 : 5 )");
        assert_eq!(parse_and_print_ast(" true  ?  1 ? 2 : 3  :  5 "), "( true ? ( 1 ? 2 : 3 ) : 5 )");
        assert_eq!(parse_and_print_ast(" true  ?  1 ? 2 : 3  :  5 ? 6 : 7 "), "( true ? ( 1 ? 2 : 3 ) : ( 5 ? 6 : 7 ) )");
        assert_eq!(parse_and_print_ast("1 ? 2 : 3 ? 4 : 5 ? 6 : 7 ? 8 : 9"), "( 1 ? 2 : ( 3 ? 4 : ( 5 ? 6 : ( 7 ? 8 : 9 ) ) ) )");
        assert_eq!(parse_and_print_ast("1 ? 2 ? 3 : 4 : 5"), "( 1 ? ( 2 ? 3 : 4 ) : 5 )");
    }

    #[test]
    fn test_expr_parser_unary() {
        assert_eq!(parse_and_print_ast(" !true"), "( ! true )");
        assert_eq!(parse_and_print_ast(" !false"), "( ! false )");
        assert_eq!(parse_and_print_ast("- 4.56"), "( - 4.56 )");
        assert_eq!(parse_and_print_ast(" -004.5600"), "( - 4.56 )");
    }

    #[test]
    fn test_expr_parser_factor() {
        assert_eq!(parse_and_print_ast("5*5"), "( 5 * 5 )");
        assert_eq!(parse_and_print_ast("10%2"), "( 10 % 2 )");
        assert_eq!(parse_and_print_ast("5/-2"), "( 5 / ( - 2 ) )");
    }

    #[test]
    fn test_expr_parser_equality() {
        assert_eq!(parse_and_print_ast(r#"5 != 5; whatever"#), r#"( 5 != 5 )"#);
        assert_eq!(parse_and_print_ast(r#"420 == 69; whatever"#), r#"( 420 == 69 )"#);
        assert_eq!(parse_and_print_ast(r#"1 == 2 != 3; whatever"#), r#"( ( 1 == 2 ) != 3 )"#);
        assert_eq!(parse_and_print_ast(r#"1 == 2 != 3 == 4; whatever"#), r#"( ( ( 1 == 2 ) != 3 ) == 4 )"#);
    }

    #[test]
    fn test_expr_parser_comparison() {
        assert_eq!(parse_and_print_ast("5 > 5 == false; whatever") ,"( ( 5 > 5 ) == false )");
        assert_eq!(parse_and_print_ast("5 >= 5; whatever") ,"( 5 >= 5 )");
        assert_eq!(parse_and_print_ast("5 <= 5; whatever") ,"( 5 <= 5 )");
        assert_eq!(parse_and_print_ast("1 < 2; whatever") ,"( 1 < 2 )");
    }

    // #[test]
    // fn test_expr_parser_errors() {
    //     assert_eq!(parse_and_print_ast("== 5") ,"( ( 5 > 5 ) == false )");
    // }
}