pub mod test_utils{

    use crate::visitor_interpreter::*;
    use lox_lexer::lexer::scan_tokens;
    use lox_library;
    use lox_library::{lox_error, Object};
    use crate::expression_tree_builder;
    use crate::interpreter_types::LoxObject;
    use crate::parser::Parser;
    use crate::statement_tree_builder;
    use crate::visitor_pretty_printer::PrettyPrinter;

    pub fn trim_leading_whitespace(text: &str) -> String {
        text.lines()
            .map( |s| s.trim_start() )
            .fold(String::new(), |a, b| a + b + "\n")
    }

    pub fn parse_and_interpret(source: &str) -> Object {
        let tokens_scanned = scan_tokens(source).unwrap();
        let expr = expression_tree_builder::parse_tokens(&tokens_scanned).unwrap();

        let mut buffer = Vec::<u8>::new();
        let mut interpreter = Interpreter::new(&mut buffer);
        match expr.accept(&mut interpreter).unwrap() {
            LoxObject::Object(obj) => obj,
            _ => Object::Nil(())
        }
    }

    pub fn parse_script(source: &str) -> Result<String, Vec<lox_error::Error>> {

        let mut buffer = Vec::<u8>::new();
        let mut interpreter = Interpreter::new(&mut buffer);

        let tokens = scan_tokens(source).unwrap();
        let mut parser = Parser::new(&tokens);
        let mut stmnts =  statement_tree_builder::parse_tokens(&mut parser)?;

        for stmnt in stmnts.iter_mut() {
            match stmnt.accept(&mut interpreter){
                Ok(_) => {},
                Err(e) => return Ok(e),
            }
        }

        Ok(String::from_utf8_lossy(&buffer).to_string())
    }

    pub fn pretty_print_script(source: &str) -> String {

        let mut printer = PrettyPrinter::new();

        let tokens = scan_tokens(source).unwrap();
        let mut parser = Parser::new(&tokens);
        let stmnts =  statement_tree_builder::parse_tokens(&mut parser).unwrap();

        let mut ret = String::new();

        for stmnt in stmnts {
            ret.push_str(&stmnt.accept(&mut printer));
            ret.push('\n');
        }

        ret
    }


}
