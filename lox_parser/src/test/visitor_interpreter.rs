
#[cfg(test)]
pub mod tests {
    use crate::visitor_interpreter::*;
    use lox_lexer::lexer::scan_tokens;
    use lox_library;
    use lox_library::Object;
    use crate::expression_tree_builder;
    use crate::interpreter_types::LoxObject;
    use crate::parser::Parser;
    use crate::statement_tree_builder;

    use crate::test::test_helper::test_utils::*;
    use super::*;

    #[test]
    fn test_maths() {
        assert_eq!(parse_and_interpret(" 1/0 "),  Object::new_num(f64::INFINITY));
        assert_eq!(parse_and_interpret(" -1/0 "), Object::new_num(f64::NEG_INFINITY));
        assert_eq!(parse_and_interpret(" -1/-0 "),Object::new_num(f64::INFINITY));
        assert_eq!(parse_and_interpret(" 1/-0 "), Object::new_num(f64::NEG_INFINITY));

        let zero_over_zero = match parse_and_interpret(" 0/0 ") {
            Object::Num(n) => n,
            _ => unreachable!(),
        };
        assert!(f64::is_nan(zero_over_zero));

        let neg_zero_over_zero = match parse_and_interpret(" -0/0 ") {
            Object::Num(n) => n,
            _ => unreachable!(),
        };
        assert!(f64::is_nan(neg_zero_over_zero));

    }

    #[test]
    fn test_string_conversion() {
        assert_eq!(parse_and_interpret(r#" ""+ true "#),          Object::new_str("true".to_string()));
        assert_eq!(parse_and_interpret(r#" ""+ false "#),         Object::new_str("false".to_string()));
        assert_eq!(parse_and_interpret(r#" ""+ false + true"#),   Object::new_str("falsetrue".to_string()));
        assert_eq!(parse_and_interpret(r#" 5 + "" + false"#),     Object::new_str("5false".to_string()));
        assert_eq!(parse_and_interpret(r#" -25 + "" + false"#),   Object::new_str("-25false".to_string()));
        assert_eq!(parse_and_interpret(r#" 10 + 20 + "" "#),      Object::new_str("30".to_string()));
        assert_eq!(parse_and_interpret(r#" "" + 10 + 20 + "" "#), Object::new_str("1020".to_string()));
    }


    #[test]
    fn test_expr_parser_primary() {
        assert_eq!(parse_and_interpret(" true"),               Object::new_bool(true)  );
        assert_eq!(parse_and_interpret(" false"),              Object::new_bool(false) );
        assert_eq!(parse_and_interpret(" 4.56"),               Object::new_num(4.56)   );
        assert_eq!(parse_and_interpret(" 004.5600"),           Object::new_num(4.56)   );
        assert_eq!(parse_and_interpret(r#"  "Hello World"  "#),Object::new_str("Hello World".to_string()));
        assert_eq!(parse_and_interpret("  nil  "),             Object::new_nil());
    }

    #[test]
    fn test_expr_parser_ternary() {
        assert_eq!(parse_and_interpret(" true  ?  4  :  5 "),Object::new_num(4.0) );
        assert_eq!(parse_and_interpret(" true  ?  true ? 2 : 3  :  5 "),Object::new_num(2.0) );
        assert_eq!(parse_and_interpret(" true  ?  false ? 2 : 3  :  5 "),Object::new_num(3.0) );
        assert_eq!(parse_and_interpret(" false  ?  true ? 2 : 3  :  5 "),Object::new_num(5.0) );
        assert_eq!(parse_and_interpret(" false  ?  false ? 2 : 3  :  5 "),Object::new_num(5.0) );
        assert_eq!(parse_and_interpret(" true  ?  true ? 2 : 3  :  true ? 6 : 7 "),Object::new_num(2.0) );
        assert_eq!(parse_and_interpret(" true  ?  true ? 2 : 3  :  false ? 6 : 7 "),Object::new_num(2.0) );
        assert_eq!(parse_and_interpret(" true  ?  false ? 2 : 3  :  true ? 6 : 7 "),Object::new_num(3.0) );
        assert_eq!(parse_and_interpret(" true  ?  false ? 2 : 3  :  false ? 6 : 7 "),Object::new_num(3.0) );
        assert_eq!(parse_and_interpret(" false  ?  true ? 2 : 3  :  true ? 6 : 7 "),Object::new_num(6.0) );
        assert_eq!(parse_and_interpret(" false  ?  true ? 2 : 3  :  false ? 6 : 7 "),Object::new_num(7.0) );
        assert_eq!(parse_and_interpret(" false  ?  false ? 2 : 3  :  true ? 6 : 7 "),Object::new_num(6.0) );
        assert_eq!(parse_and_interpret(" false  ?  false ? 2 : 3  :  false ? 6 : 7 "),Object::new_num(7.0) );

    }

    #[test]
    fn test_expr_parser_unary() {
        assert_eq!(parse_and_interpret(" !true"),Object::new_bool(false) );
        assert_eq!(parse_and_interpret(" !false"),Object::new_bool(true) );
        assert_eq!(parse_and_interpret("- 4.56"), Object::new_num(-4.56 ));
        assert_eq!(parse_and_interpret(" -004.5600"), Object::new_num(-4.56 ));
    }

    #[test]
    fn test_expr_parser_factor() {
        assert_eq!(parse_and_interpret("5*5"),Object::new_num(25.0));
        assert_eq!(parse_and_interpret("5/-2"), Object::new_num(-2.5));
        assert_eq!(parse_and_interpret("10%2"), Object::new_num(0.0));
    }

    #[test]
    fn test_expr_parser_equality() {
        assert_eq!(parse_and_interpret(r#"5 != 5; whatever"#),Object::new_bool(false) );
        assert_eq!(parse_and_interpret(r#"420 == 69; whatever"#),Object::new_bool(false) );
        assert_eq!(parse_and_interpret(r#"1 == 2 != false; whatever"#),Object::new_bool(false) );
        assert_eq!(parse_and_interpret(r#"(1 == 2) != (3 == 4); whatever"#),Object::new_bool(false) );
    }

    #[test]
    fn test_expr_parser_comparison() {
        assert_eq!(parse_and_interpret("5 > 5 == false; whatever"),Object::new_bool(true) );
        assert_eq!(parse_and_interpret("5 >= 5; whatever"),Object::new_bool(true) );
        assert_eq!(parse_and_interpret("5 <= 5; whatever"),Object::new_bool(true) );
        assert_eq!(parse_and_interpret("1 < 2; whatever"),Object::new_bool(true) );
    }


    #[test]
    fn test_print_statement() {
        assert_eq!(parse_script(r#"print 42;"#).unwrap(), "42\n" );
        assert_eq!(parse_script(r#"var x = 42; print x;"#).unwrap(), "42\n" );
        assert_eq!(parse_script(r#"var x = 1; var y = 2; print x+y;"#).unwrap(), "3\n" );
        assert_eq!(parse_script(r#"var x = 1; var y = "2"; print x+y;"#).unwrap(), "12\n" );
    }

    #[test]
    fn test_assignment_0() {
        let script = r#"
        var x;
        var y;
        var z;
        var a = x = y = z = "a";
        print a + x + y + z; "#;

        assert_eq!(parse_script(script).unwrap(), "aaaa\n" );
    }

    #[test]
    fn test_assignment_1() {
        let script = r#"
        var x;
        var y;
        var z;
        var z = (x=40) * (y=1.5);
        print x;
        print y;
        print z;
        "#;

        assert_eq!(parse_script(script).unwrap(), "40\n1.5\n60\n" );
    }
    #[test]
    fn test_scopes() {
        let script = r#"
        var x = 1;
        var y = 2;
        {
            print x + y;
            var x = 10;
            print x + y;
        }
        print x + y;
        "#;

        assert_eq!(parse_script(script).unwrap(), "3\n12\n3\n" );
    }

    #[test]
    fn test_scopes_2() {
        let script = r#"
        var a = "global a";
        var b = "global b";
        var c = "global c";
        {
            var a = "outer a";
            var b = "outer b";
            {
                var a = "inner a";
                print a;
                print b;
                print c;
            }
            print a;
            print b;
            print c;
        }
        print a;
        print b;
        print c;"#;

        let result = r#"inner a
                        outer b
                        global c
                        outer a
                        outer b
                        global c
                        global a
                        global b
                        global c"#;

        assert_eq!(parse_script(script).unwrap(), trim_leading_whitespace(result));
    }


    #[test]
    fn test_scope_assign_to_self() {
        let script = r#"
        var a = "Hello ";
        {
            var a = a + "World";
            print a;
        }
        print a;
        "#;

        let result = r#"Hello World
                        Hello "#;

        assert_eq!(parse_script(script).unwrap(), trim_leading_whitespace(result));
    }

    #[test]
    fn test_if() {
        let script = r#"
        var a = true;
        if ( a ) {
            print "Good";
        } else {
            print "Bad";
        }
        a = false;
        if ( a ) {
            print "Good";
        } else {
            print "Bad";
        }
        "#;

        let result = r#"Good
                        Bad"#;

        assert_eq!(parse_script(script).unwrap(), trim_leading_whitespace(result));
    }



    #[test]
    fn empty_statements(){
        parse_script(";;;;;").unwrap();
        assert_eq!(parse_script("if (false);   else {print \"Hello\";}").unwrap(), "Hello\n");
        assert_eq!(parse_script("if (false) {print \"XXX\";} else {print \"Hello\";}").unwrap(), "Hello\n");
    }

}
