#[cfg(test)]
mod tests_logic {
    use crate::visitor_interpreter::*;
    use lox_lexer::lexer::scan_tokens;
    use lox_library;
    use lox_library::Object;
    use crate::expression_tree_builder;
    use crate::parser::Parser;
    use crate::statement_tree_builder;

    use crate::test::test_helper::test_utils::*;


    #[test]
    fn test_fibonnaci_generation_for() {
        let script = r#"
            var a = 1;
            var temp;

            for (var b = 1; a < 10000; b = temp + b) {
              print a;
              temp = a;
              a = b;
            }
        "#;

        let result = r#"1
                        1
                        2
                        3
                        5
                        8
                        13
                        21
                        34
                        55
                        89
                        144
                        233
                        377
                        610
                        987
                        1597
                        2584
                        4181
                        6765"#;

        assert_eq!(parse_script(script).unwrap(), trim_leading_whitespace(result));

        let transformed_source =
r#"var a = ( 1 ) ;
var temp ;
{
  var b = ( 1 ) ;
  while ( ( a < 10000 ) ) {
    {
      {
        print a ;
        ( temp = a );
        ( a = b );

      }
      ( b = ( temp + b ) );

    }
  }

}
"#;
        assert_eq!(pretty_print_script(script), transformed_source);

    }


    #[test]
    fn test_for_with_break() {
        let script = r#"
                for (var A = 1; ; A = A+1) {
                  print A;
                  if ( A == 5 ) break;
                  print "Hello";
                }
            "#;

        let result = r#"1
                        Hello
                        2
                        Hello
                        3
                        Hello
                        4
                        Hello
                        5"#;

        assert_eq!(parse_script(script).unwrap(), trim_leading_whitespace(result));
    }

    #[test]
    fn test_for_triangle() {
        let script = r#"
                for (var i = 0; i < 10; i = i+1) {
                    var line = "";
                    for (var j = 0; ; j = j+1) {
                        line = line + "*";
                        if (j == i)
                            break;
                    }
                    print line;
                }
            "#;

        let result = r#"*
                        **
                        ***
                        ****
                        *****
                        ******
                        *******
                        ********
                        *********
                        **********"#;

        assert_eq!(parse_script(script).unwrap(), trim_leading_whitespace(result));
    }

    #[test]
    fn test_bad_break() {
        let script = r#"
                    print "Hello";
                    break;
                    print "World";
            "#;

        let parsed = parse_script(script);
        assert!(parsed.is_err());
        let err = parsed.unwrap_err();
        assert_eq!(err.len(), 1);
        let err_string = &err[0].error_string;
        assert_eq!(err_string, "break can only be used within a loop");
    }

    #[test]
    fn test_for_with_continue() {
        let script = r#"
                var A = 0;
                while (A < 10) {
                  A = A + 1;
                  if ( A%2 == 1 )
                    continue;
                  print A;
                }
            "#;

        let result = r#"2
                        4
                        6
                        8
                        10"#;

        println!("{}", pretty_print_script(script));
        assert_eq!(parse_script(script).unwrap(), trim_leading_whitespace(result));
    }

}
