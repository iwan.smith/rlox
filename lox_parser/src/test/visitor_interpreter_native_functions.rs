#[cfg(test)]
mod tests_logic {
    use crate::visitor_interpreter::*;
    use lox_lexer::lexer::scan_tokens;
    use lox_library;
    use lox_library::Object;
    use crate::expression_tree_builder;
    use crate::parser::Parser;
    use crate::statement_tree_builder;

    use crate::test::test_helper::test_utils::*;


    #[test]
    fn test_42() {
        let script = r#"
            print get_42();
            "#;

        let result = r#"42"#;

        assert_eq!(parse_script(script).unwrap(), trim_leading_whitespace(result));

    }

    #[test]
    fn test_sleep_literal() {
        let script = r#"
            var t0 = clock();
            sleep(0.01);
            var t1 = clock();
            print t1-t0;
            "#;

        let mut time_taken = parse_script(script).unwrap();
        time_taken.pop();

        println!("Script ran in {} s", time_taken);

        let time_taken = time_taken.parse::<f64>().unwrap();

        assert!(time_taken > 0.01);

        // allow an overhead of 50 micros to execute the code
        assert!(time_taken < 0.0105);

    }

    #[test]
    fn test_sleep_variable() {
        let script = r#"
            var t0 = clock();
            var sleep_time = 0.01;
            sleep(sleep_time);
            var t1 = clock();
            print t1-t0;
            "#;

        let mut time_taken = parse_script(script).unwrap();
        time_taken.pop();

        println!("Script ran in {} s", time_taken);

        let time_taken = time_taken.parse::<f64>().unwrap();

        assert!(time_taken > 0.01);

        // allow an overhead of 50 micros to execute the code
        assert!(time_taken < 0.0105);

    }

}
