#[cfg(test)]
mod tests_logic {
    use crate::visitor_interpreter::*;
    use lox_lexer::lexer::scan_tokens;
    use lox_library;
    use lox_library::Object;
    use crate::expression_tree_builder;
    use crate::parser::Parser;
    use crate::statement_tree_builder;

    use crate::test::test_helper::test_utils::*;


    #[test]
    fn test_43() {
        let script = r#"
            fun get_43(){
                print 43;
             }
             get_43();
            "#;

        let result = r#"43"#;

        assert_eq!(parse_script(script).unwrap(), trim_leading_whitespace(result));
    }

    #[test]
    fn test_function_assignment() {
        let script = r#"
            fun get_43(){
                print 43;
             }

             var f = get_43;

             f();
            "#;

        let result = r#"43"#;

        assert_eq!(parse_script(script).unwrap(), trim_leading_whitespace(result));
    }

    #[test]
    fn test_function_argument() {
        let script = r#"
            fun printer(to_print){
                print to_print;
                return to_print;
             }

             for( var x = 0; x < 10; x=x+1){
                 printer(x);
             }
             print printer(10);
            "#;

        let result = "0\n1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n10\n";
        assert_eq!(parse_script(script).unwrap(), trim_leading_whitespace(result));
    }

    #[test]
    fn test_fibonacci() {
        let script = r#"
            fun fibonacci(i){
                if ( i < 2 ) {
                    return 1;
                } else {
                    return fibonacci(i-1) + fibonacci(i-2);
                }
             }

             for (var j = 0; j < 10; j = j+1){
                 print fibonacci(j);
             }
            "#;

        let result = "1\n1\n2\n3\n5\n8\n13\n21\n34\n55";
        assert_eq!(parse_script(script).unwrap(), trim_leading_whitespace(result));
    }


    #[test]
    fn test_function_scoping() {
        let script = r#"
            fun printer(){
                print non_global_counter;
             }

             for( var x = 0; x < 10; x=x+1){
                 var non_global_counter = x;
                 printer();
             }
            "#;

        let result = "Variable: non_global_counter is undeclared!";
        assert_eq!(parse_script(script).unwrap(), result);

    }

}
