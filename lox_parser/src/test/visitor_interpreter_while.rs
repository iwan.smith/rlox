#[cfg(test)]
mod tests_logic {
    use crate::visitor_interpreter::*;
    use lox_lexer::lexer::scan_tokens;
    use lox_library;
    use lox_library::Object;
    use crate::expression_tree_builder;
    use crate::parser::Parser;
    use crate::statement_tree_builder;

    use crate::test::test_helper::test_utils::*;


    #[test]
    fn test_fibonnaci_generation() {
        let script = r#"
            var a = 0;
            var b = 1;
            while ( b < 4181 ) {
                var next = a+b;
                a = b;
                b = next;
                print next;
            }
        "#;

        let result = r#"1
                        2
                        3
                        5
                        8
                        13
                        21
                        34
                        55
                        89
                        144
                        233
                        377
                        610
                        987
                        1597
                        2584
                        4181"#;

            assert_eq!(parse_script(script).unwrap(), trim_leading_whitespace(result));

        }
}
