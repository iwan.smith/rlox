
#[cfg(test)]
mod tests {
    use crate::visitor_pretty_printer::*;
    use lox_lexer::lexer::scan_tokens;
    use crate::expressions::{Binary, Group, Literal, Unary, Expression};


    #[test]
    fn test_visit_literal() {
        
        
        let tokens_scanned = scan_tokens(r#"5 4.67 true "Hello World" "#).unwrap();
        {
            let l = Literal { expr: tokens_scanned[0].clone() };
            let l = Expression::Literal(l);
            assert_eq!(l.accept(&mut PrettyPrinter::new()), "5");
        }
        {
            let l = Literal { expr: tokens_scanned[1].clone() };
            let l = Expression::Literal(l);
            assert_eq!(l.accept(&mut PrettyPrinter::new()), "4.67");
        }
        // {
        //     let l = Literal { expression: tokens_scanned[2].clone() };
        //     assert_eq!(l.accept(&mut PrettyPrinter::new()).unwrap(), "true");
        // }
        {
            let l = Literal { expr: tokens_scanned[3].clone() };
            let l = Expression::Literal(l);
            assert_eq!(l.accept(&mut PrettyPrinter::new()), "\"Hello World\"");
        }
    }

    #[test]
    fn test_visit_unary() {
        let tokens_scanned = scan_tokens(r#"+4 -2 !"Hello""#).unwrap();
        {
            let operator = tokens_scanned[0].clone();
            let right = Box::new(Expression::Literal(Literal{ expr: tokens_scanned[1].clone()}));
            let u = Unary {
                operator: operator.clone(),
                right
            };
            let u = Expression::Unary(u);
            assert_eq!(u.accept(&mut PrettyPrinter::new()), "( + 4 )");
        }

        {
            let operator = tokens_scanned[2].clone();
            let right = Box::new(Expression::Literal(Literal{ expr: tokens_scanned[3].clone()}));
            let u = Unary {
                operator: operator.clone(),
                right
            };
            let u = Expression::Unary(u);
            assert_eq!(u.accept(&mut PrettyPrinter::new()), "( - 2 )");
        }

        {
            let operator = &tokens_scanned[4];
            let right = Box::new(Expression::Literal(Literal{ expr: tokens_scanned[5].clone() }));
            let u = Unary {
                operator: operator.clone(),
                right
            };
            let u = Expression::Unary(u);
            assert_eq!(u.accept(&mut PrettyPrinter::new()), r#"( ! "Hello" )"#);
        }
    }



    #[test]
    fn test_visit_binary() {
        let tokens_scanned = scan_tokens(r#"2+4 7-2 6*9"#).unwrap();
        {
            let left = Box::new(Expression::Literal(Literal{ expr:tokens_scanned[0].clone()}));
            let operator = tokens_scanned[1].clone();
            let right = Box::new(Expression::Literal(Literal{ expr:tokens_scanned[2].clone()}));
            let b = Binary {
                left,
                operator,
                right
            };
            let b = Expression::Binary(b);
            assert_eq!(b.accept(&mut PrettyPrinter::new()), "( 2 + 4 )");
        }
        {
            let left = Box::new(Expression::Literal(Literal{ expr:tokens_scanned[3].clone()}));
            let operator = tokens_scanned[4].clone();
            let right = Box::new(Expression::Literal(Literal{ expr:tokens_scanned[5].clone()}));
            let b = Binary {
                left,
                operator,
                right
            };
            let b = Expression::Binary(b);
            assert_eq!(b.accept(&mut PrettyPrinter::new()), "( 7 - 2 )");
        }
        {
            let left = Box::new(Expression::Literal(Literal{ expr:tokens_scanned[6].clone()}));
            let operator = tokens_scanned[7].clone();
            let right = Box::new(Expression::Literal(Literal{ expr:tokens_scanned[8].clone()}));
            let b = Binary {
                left,
                operator,
                right
            };
            let b = Expression::Binary(b);
            assert_eq!(b.accept(&mut PrettyPrinter::new()), "( 6 * 9 )");
        }
    }

    #[test]
    fn test_from_book() {
        let ts = scan_tokens(" * - 123 45.67").unwrap();


        let left = {
            let right = Box::new(Expression::Literal(Literal{ expr:ts[2].clone()}));
            Box::new(Expression::Unary(Unary{operator:ts[1].clone(), right}))
        };

        let right = {
            let expression = Box::new(Expression::Literal(Literal{ expr:ts[3].clone()}));
            Box::new(Expression::Group(Group{ expr: expression }))
        };

        let operator = ts[0].clone();

        let expr = Expression::Binary( Binary {
            left,
            operator,
            right,
        });
        assert_eq!(expr.accept(&mut PrettyPrinter::new()), "( ( - 123 ) * ( group 45.67 ) )");

    }

    }

