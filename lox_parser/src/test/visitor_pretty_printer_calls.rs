#[cfg(test)]
mod tests_logic {
    use crate::visitor_interpreter::*;
    use lox_lexer::lexer::scan_tokens;
    use lox_library;
    use lox_library::Object;
    use crate::expression_tree_builder;
    use crate::parser::Parser;
    use crate::statement_tree_builder;

    use crate::test::test_helper::test_utils::*;


    #[test]
    fn test_print_function_call() {
        let script = "function(1,2,3);";


        let transformed_source =
            "function( 1, 2, 3 );\n";
        assert_eq!(pretty_print_script(script), transformed_source);
    }
}