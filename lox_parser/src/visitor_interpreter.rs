use std::iter::zip;
use std::mem::swap;
use crate::expressions;
use crate::statements;
use lox_library;
use crate::expressions::{Assignment, Call, Variable};
use crate::environment::Environment;
use crate::interpreter_types::LoxObject;
use crate::statements::{Continue, FunctionDecl, Return};

type TT = lox_library::token::TokenType;

pub struct Interpreter<'buf, 'fundecl>{
    env: Environment<'buf, 'fundecl>,
    break_from_while: bool,
    continue_from_while: bool,
    returning_from_function: bool,
    return_value : LoxObject<'fundecl>
}

impl Interpreter<'_, '_>{
    pub fn new(buffer: &mut dyn std::io::Write) -> Interpreter{
        let break_from_while = false;
        let continue_from_while = false;
        let returning_from_function = false;
        let env = Environment::new(buffer);
        Interpreter{env, break_from_while, continue_from_while, returning_from_function, return_value: LoxObject::new_nil() }
    }

    pub fn is_in_fast_forward(&self) -> bool {
        return self.continue_from_while || self.break_from_while || self.returning_from_function;
    }

}

impl<'fundecl> expressions::VisitorExpr<Result<LoxObject<'fundecl>,String>> for Interpreter<'_, 'fundecl>{

    fn visit_assignment(&mut self, ass: &Assignment) -> Result<LoxObject<'fundecl>, String> {
        let name = &ass.name;
        let val = ass.value.accept(self)?;

        match self.env.set(&name.token_name, val) {
            Some(obj) => Ok(obj.clone()),
            None => Err("trying to assign to uninitialized variable".to_string())
        }
    }

    fn visit_ternary(&mut self , ternary: &expressions::Ternary) -> Result<LoxObject<'fundecl>, String> {
        let l = ternary.left.accept(self)?;
        match l.is_truthy(){
            true  => ternary.middle.accept(self),
            false => ternary.right.accept(self),
        }
    }

    fn visit_binary(&mut self , binary: &expressions::Binary) -> Result<LoxObject<'fundecl>, String> {
        let l = match binary.left .accept(self)?{
            LoxObject::Object(obj) => obj,
            LoxObject::FunctionDeclared(_) => return Err("Left hand side of binary operator can't be a function".to_string()),
            LoxObject::FunctionNative(_)   => return Err("Left hand side of binary operator can't be a function".to_string())
        };

        let r = match binary.right.accept(self)?{
            LoxObject::Object(obj) => obj,
            LoxObject::FunctionDeclared(_) => return Err("Right hand side of binary operator can't be a function".to_string()),
            LoxObject::FunctionNative(_)   => return Err("Right hand side of binary operator can't be a function".to_string())
        };

        let ret = match binary.operator.token_type{
            TT::BangEqual    => l.neq  (&r),
            TT::EqualEqual   => l.eq   (&r),
            TT::Greater      => l.gt   (&r),
            TT::GreaterEqual => l.gt_eq(&r),
            TT::Less         => l.lt   (&r),
            TT::LessEqual    => l.lt_eq(&r),
            TT::Plus         => l + r,
            TT::Minus        => l - r,
            TT::Star         => l * r,
            TT::Slash        => l / r,
            TT::Remainder    => l % r,
            _                => Err(format!("Can't interperet binary expression {} {} {}", l, binary.operator.token_name, r))
        };

        Ok(LoxObject::Object(ret?))
    }

    fn visit_call(&mut self, call: &Call) -> Result<LoxObject<'fundecl>, String> {

        let callee = call.callee.accept(self)?;

        let arguments = call.arguments.iter().fold(Ok::<Vec<LoxObject>,String>(Vec::new()), |ret, arg| {
            let mut ret = ret?;
            ret.push(arg.accept(self)?);
            Ok(ret)
        })?;

        match callee {
            LoxObject::Object(_)           => return Err("Objects aren't callable".to_string()),
            LoxObject::FunctionNative(f)   => return f.call(arguments),
            LoxObject::FunctionDeclared(_) => {},
        };

        let mut ret : LoxObject = LoxObject::new_nil();

        if let LoxObject::FunctionDeclared(f) = callee {
            self.env.push_env();

            for (param, arg) in zip(&f.function.params, arguments){
                self.env.define(&param.token_name, arg);
            }

            //todo
            f.function.body.accept(self)?;
            swap(&mut ret, &mut self.return_value);

            self.returning_from_function = false;

            self.env.pop_env();
        }
        Ok(ret)
    }


    fn visit_grouping(&mut self , grouping: &expressions::Group) -> Result<LoxObject<'fundecl>, String> {
        grouping.expr.accept(self)
    }

    fn visit_literal(&mut self , literal: &expressions::Literal) -> Result<LoxObject<'fundecl>, String> {
        match literal.expr.representation.as_ref(){
            Some(rep) => Ok(LoxObject::Object(rep.clone())),
            None      => Err("Token does not contain a value".to_string())
        }
    }

    fn visit_logical(&mut self , logical: &expressions::Logical) -> Result<LoxObject<'fundecl>, String> {

        let l = logical.left.accept(self)?;

        if logical.operator.token_type == TT::Or {
            if l.is_truthy() {
                return Ok(l);
            }
        } else {
            if !l.is_truthy() {
                return Ok(l);
            }
        }
        logical.right.accept(self)
    }

    fn visit_unary(&mut self , unary: &expressions::Unary) -> Result<LoxObject<'fundecl> , String> {

        let r = match unary.right.accept(self)?{
            LoxObject::Object(obj) => obj,
            LoxObject::FunctionDeclared(_) => return Err("Right hand side of unary operator can't be a function".to_string()),
            LoxObject::FunctionNative(_)   => return Err("Right hand side of unary operator can't be a function".to_string())
        };

        let ret = match unary.operator.token_type{
            TT::Minus => -r,
            TT::Bang  => !r,
            _         => Err(format!("Can't treat '{}' as a unary operator: {}{}", unary.operator.token_name, unary.operator.token_name, r))
        };

        Ok(LoxObject::Object(ret?))
    }

    fn visit_variable(&mut self , var: &Variable) -> Result<LoxObject<'fundecl>, String> {

        match self.env.get(&var.name.token_name){
            Some(v)  => Ok(v.clone()),
            None => Err(format!("Variable: {} is undeclared!", var.name.token_name))
        }
    }
}


impl<'ast> statements::VisitorStmnt<'ast, Result<(), String>> for Interpreter<'_, 'ast, > {

    fn visit_empty(&mut self, _: &'ast statements::Empty) -> Result<(), String> {
        Ok(())
    }

    fn visit_if(&mut self, if_stmnt: &'ast statements::If) -> Result<(), String> {
        if self.is_in_fast_forward() {
            return Ok(());
        }

        let expr_result = if_stmnt.condition.accept(self)?;
        if expr_result.is_truthy(){
            if_stmnt.then_branch.accept(self)
        } else if if_stmnt.else_branch.is_some() {
            if_stmnt.else_branch.as_ref().unwrap().accept(self)
        } else {
            Ok(())
        }
    }

    fn visit_vardecl(&mut self, var: &statements::VarDecl) -> Result<(), String> {
        if self.is_in_fast_forward() {
            return Ok(());
        }

        let var_name = &var.name.token_name;
        let var_val = match &var.expr {
            Some(v) => v.accept(self)?,
            None    => LoxObject::new_nil()
        };

        self.env.define( var_name,var_val );

        Ok(())
    }

    fn visit_expr(&mut self, e: &statements::Expr) -> Result<(), String> {
        if self.is_in_fast_forward() {
            return Ok(());
        }

        e.expr.accept( self )?;
        Ok(())
    }
    fn visit_print(& mut self, e: &statements::Print) -> Result<(), String> {
        if self.is_in_fast_forward() {
            return Ok(());
        }

        let result = e.expr.accept( self )?;

        match self.env.print(&result) {
            Ok(()) => Ok(()),
            Err(err) => { println!("{}", err.to_string()); Ok(()) }
        }

    }

    fn visit_return(&mut self, e: &'ast Return) -> Result<(), String> {
        if self.is_in_fast_forward() {
            return Ok(());
        };

        self.return_value = e.expr.accept( self )?;

        self.returning_from_function = true;
        Ok(())
    }

    fn visit_while(&mut self, while_loop: &'ast statements::While) -> Result<(), String> {
        while while_loop.condition.accept(self)?.is_truthy() {
            while_loop.body.accept(self)?;
            if self.break_from_while{
                break;
            }
            self.continue_from_while = false;
        }
        self.break_from_while = false;
        Ok(())
    }

    fn visit_break(&mut self, _: &'ast statements::Break) -> Result<(), String> {
        if self.is_in_fast_forward() {
            return Ok(());
        }

        self.break_from_while = true;
        Ok(())
    }

    fn visit_continue(&mut self, _: &'ast Continue) -> Result<(), String> {
        if self.is_in_fast_forward() {
            return Ok(());
        }

        self.continue_from_while = true;
        Ok(())
    }

    fn visit_block(&mut self, block: &'ast statements::Block) -> Result<(), String> {
        if self.is_in_fast_forward() {
            return Ok(());
        }

        self.env.push_scope();
        for stmnt in &block.stmnts {
            stmnt.accept(self)?
        }
        self.env.pop_scope();
        Ok(())
    }

    fn visit_function_decl(&mut self, fun: &'ast FunctionDecl) -> Result<(), String> {
        if self.is_in_fast_forward() {
            return Ok(());
        }

        let fun_arity = fun.params.len();
        let fun_name  = &fun.name.token_name;
        let func = LoxObject::new_declared_function(fun, fun_arity);
        self.env.define( fun_name, func );
        Ok(())
    }
}
