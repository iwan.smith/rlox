use crate::expressions::{Assignment, Call, Variable};
use crate::statements::{Continue, FunctionDecl, If, Return, Statement, While};
use crate::{expressions, statements};

pub struct PrettyPrinter{
    indentation: u32,
}
impl PrettyPrinter{
    pub fn new() -> PrettyPrinter {
        PrettyPrinter{indentation:0}
    }
}

impl expressions::VisitorExpr<String> for PrettyPrinter {
    fn visit_assignment(&mut self, ass: &Assignment) -> String {
        let l = &ass.name.token_name;
        let r = ass.value.accept(self);
        format!("( {} = {} )", l, r)
    }

    fn visit_ternary(&mut self, ternary: &expressions::Ternary) -> String {
        let l = ternary.left.accept(self);
        let m = ternary.middle.accept(self);
        let r = ternary.right.accept(self);
        format!("( {} ? {} : {} )", l, m, r)
    }

    fn visit_binary(&mut self, binary: &expressions::Binary) -> String {
        let l = binary.left.accept(self);
        let r = binary.right.accept(self);
        format!("( {} {} {} )", l, binary.operator.token_type.to_string(), r)
    }

    fn visit_call(&mut self, call: &Call) -> String {

        let function = call.callee.accept(self);

        let arguments = call.arguments.iter().fold(String::new(), |mut ret, elem| {
            if ret.len() > 0 {
                ret.push_str(", ");
            }
            ret.push_str(&*elem.accept(self));
            ret
        });

        format!("{}( {} )", function, arguments)
    }

    fn visit_grouping(&mut self, grouping: &expressions::Group) -> String {
        let g = grouping.expr.accept(self);
        format!("( group {} )", g)
    }

    fn visit_literal(&mut self, literal: &expressions::Literal) -> String {
        match literal.expr.representation.as_ref() {
            Some(rep) => format!("{}", rep),
            None => literal.expr.token_name.clone(),
        }
    }

    fn visit_logical(&mut self, binary: &expressions::Logical) -> String {
        let l = binary.left.accept(self);
        let r = binary.right.accept(self);
        format!("( {} {} {} )", l, &binary.operator.token_type.to_string(), r)
    }

    fn visit_unary(&mut self, unary: &expressions::Unary) -> String {
        let r = unary.right.accept(self);
        format!("( {} {} )", &unary.operator.token_type.to_string(), r)
    }

    fn visit_variable(&mut self, var: &Variable) -> String {
        format!("{}", &var.name.token_name)
    }
}

impl statements::VisitorStmnt<'_, String> for PrettyPrinter {
    fn visit_empty(&mut self, _: &statements::Empty) -> String {
        "".to_string()
    }

    fn visit_if(&mut self, if_stmnt: &If) -> String {
        let indnt = " ".repeat(self.indentation as usize);
        self.indentation += 2;

        let ret = if if_stmnt.else_branch.is_some() {
            format!(
                "{indnt}if ( {} ) {{\n{}\n{indnt} }} else {{\n{}\n{indnt} }}\n",
                if_stmnt.condition.accept(self),
                if_stmnt.then_branch.accept(self),
                if_stmnt.else_branch.as_ref().unwrap().accept(self),
            )
        } else {
            format!(
                "{indnt}if ( {} ) {{\n{}\n{indnt}}}\n",
                if_stmnt.condition.accept(self),
                if_stmnt.then_branch.accept(self),
            )
        };
        assert!(self.indentation >= 2);
        self.indentation -= 2;
        ret
    }

    fn visit_vardecl(&mut self, vardecl: &statements::VarDecl) -> String {
        let indnt = " ".repeat(self.indentation as usize);

        match &vardecl.expr {
            Some(v) => format!("{indnt}var {} = ( {} ) ;", &vardecl.name.token_name, v.accept(self)),
            _ => format!("{indnt}var {} ;", &vardecl.name.token_name),
        }
    }

    fn visit_expr(&mut self, expr: &statements::Expr) -> String {
        let indnt = " ".repeat(self.indentation as usize);

        format!("{indnt}{};", expr.expr.accept(self))
    }

    fn visit_print(&mut self, print: &statements::Print) -> String {
        let indnt = " ".repeat(self.indentation as usize);

        format!("{indnt}print {} ;", print.expr.accept(self))
    }

    fn visit_return(&mut self, ret: &statements::Return) -> String {
        let indnt = " ".repeat(self.indentation as usize);
        format!("{indnt}return {} ;", ret.expr.accept(self))
    }

    fn visit_while(&mut self, while_loop: &While) -> String {
        let indnt = " ".repeat(self.indentation as usize);
        self.indentation += 2;

        let ret = format!(
            "{indnt}while ( {} ) {{\n{}\n{indnt}}}",
            while_loop.condition.accept(self),
            while_loop.body.accept(self)
        );

        assert!(self.indentation>=2);
        self.indentation -= 2;

        ret
    }

    fn visit_break(&mut self, _: &statements::Break) -> String {
        let indnt = " ".repeat(self.indentation as usize);
        format!("{indnt}break;")
    }

    fn visit_continue(&mut self, _: &Continue) -> String {
        let indnt = " ".repeat(self.indentation as usize);
        format!("{indnt}continue;")
    }

    fn visit_block(&mut self, block: &statements::Block) -> String {
        let indnt = " ".repeat(self.indentation as usize);
        self.indentation += 2;

        let mut stmnts_str = String::new();
        for stmnt in &block.stmnts {
            stmnts_str.push_str(&*format!("{}\n", stmnt.accept(self)))
        }

        let ret = format!("{indnt}{{\n{}\n{indnt}}}", stmnts_str);

        assert!(self.indentation>=2);
        self.indentation -= 2;
        ret

    }

    fn visit_function_decl(&mut self, _: &FunctionDecl) -> String {
        todo!()
    }
}
