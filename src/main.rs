extern crate core;

use std::{env, fs, io};
use std::io::{BufWriter, Write};
use lox_library::token;
use lox_lexer::lexer;
use lox_library::lox_error::Error;
use lox_parser::{statement_tree_builder, visitor_pretty_printer, parser::Parser};
use lox_parser::visitor_interpreter;
use lox_parser::expression_tree_builder;


fn run(source: &String, mut interp : visitor_interpreter::Interpreter) ->Result<(),Vec<Error>>{
    let tokens = lexer::scan_tokens(source).unwrap();
    let mut parser = Parser::new(&tokens);
    let mut interp = interp;
    let stmnts =  statement_tree_builder::parse_tokens(&mut parser)?;

    for stmnt in stmnts.iter() {

        let parsed_stmnt = stmnt.accept(&mut visitor_pretty_printer::PrettyPrinter::new());
        println!("{}", parsed_stmnt);

        let parsed_stmnt = stmnt.accept(&mut interp).unwrap();
    }
    Ok(())
}

fn run_file(input_file_name: &str) -> Result<(), String> {
    println!("Processing file: {}", input_file_name);

    let file_bytes = match fs::read_to_string(input_file_name){
        Ok(bytes) => bytes,
        Err(e) => return Err(e.to_string())
    };

    let mut buffer = std::io::stdout();
    let mut interpreter = visitor_interpreter::Interpreter::new(&mut buffer);

    run(&file_bytes, interpreter).unwrap();

    // if hadError exit(65)
    Ok(())
}

// fn run_prompt() -> io::Result<()> {
//
//     let mut buffer = &mut std::io::stdout();
//     let mut interpreter = visitor_interpreter::Interpreter::new(&mut buffer);
//
//     let mut line = String::new();
//     let stdin = io::stdin();
//     loop {
//         line.clear();
//         print!(" > ");
//         io::stdout().flush()?;
//         if stdin.read_line(&mut line)? == 0 {
//             // check for EOF
//             return Ok(());
//         }
//         match run(&line, interpreter) {
//             Err(e) => for err in e {println!("{}", err.error_string)},
//             _ => ()
//         };
//         // hadError = false;
//
//     }
// }

fn main() -> Result<(), ()> {

    let args: Vec<_> = env::Args::collect(env::args());

    if args.len() > 2 {
        println!("Usage: rLox [script]");
    } else if args.len() == 2 {
        run_file(&args[1]).expect("Panicked wile running a file");
    } else {
        // run_prompt().expect("Panicked while running REPL");
    }

    Ok(())
}

// https://www.craftinginterpreters.com/functions.html#return-statements